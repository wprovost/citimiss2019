package com.citi.trading;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.pricing.Pricing;
import com.citi.trading.pricing.PricingTest;

/**
 * Unit test for the {@link Investor} class.
 * We provide a mock {@link Market} to accept outbound orders,
 * and we simulate fills, partial fills, and rejections by calling the 
 * registered <strong>Consumer</strong> directly.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=InvestorTest.Config.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class InvestorTest {

	@Configuration
	public static class Config {
		
		@Bean
		public OrderPlacer mockMarket() {
			return new MockMarket();
		}
		
		@Bean
		public Pricing.Connector connector() {
			return mock(Pricing.Connector.class);
		}
		
		@Bean
		public Pricing pricing() {
			return new Pricing();
		}
		
		@Bean
		public Investor investor() {
			return new Investor();
		}
	}
	
	public static final String MOCK_PRICING_TEMPLATE = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:41:45.620,0.0000,0.0000,0.0000,%1.4f,100";

	private static Trade pendingTrade;
	private static Consumer<Trade> pendingCallback;

	public static class MockMarket implements OrderPlacer {
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			pendingTrade = order;
			pendingCallback = callback;
		}
	}
	
	@Autowired
	private Investor investor;
	
	@Autowired
	private Pricing.Connector connector;
	
	private void preparePrice(String stock, double price) {
		when(connector.apply(PricingTest.SERVICE_URL + stock + "?periods=1"))
				.thenReturn(new ByteArrayInputStream
						(String.format(MOCK_PRICING_TEMPLATE, price).getBytes()));
	}
	
	private void buyAndConfirm(String stock, int shares, double price) {
		preparePrice(stock, price);
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndConfirm(String stock, int shares, double price, int partConfirmed) {
		preparePrice(stock, price);
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void buyAndReject(String stock, int shares, double price) {
		preparePrice(stock, price);
		investor.buy(stock, shares);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price) {
		preparePrice(stock, price);
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.FILLED);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndConfirm(String stock, int shares, double price, int partConfirmed) {
		preparePrice(stock, price);
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.PARTIALLY_FILLED);
		pendingTrade.setSize(partConfirmed);
		pendingCallback.accept(pendingTrade);
	}
	
	private void sellAndReject(String stock, int shares, double price) {
		preparePrice(stock, price);
		investor.sell(stock, shares);
		pendingTrade.setResult(Trade.Result.REJECTED);
		pendingTrade.setSize(0);
		pendingCallback.accept(pendingTrade);
	}
	
	@Test
	public void testBuy() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 100, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(100)));
		assertThat(investor.getCash(), closeTo(30000.0, 0.0001));
	}
	
	@Test
	public void testSell() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9000)));
		assertThat(investor.getCash(), closeTo(100000.0, 0.0001));
	}

	@Test
	public void testSellOut() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 10000, 100);
		assertThat(investor.getPortfolio(), not(hasKey(equalTo("MSFT"))));
	}

	/**
	 * Since we now rely on asynchronous pricing notifications, we can't
	 * pre-check that we have enough cash for the transaction.
	 * It's even possible to overdraw the account!
	 * We could do an initial, "pre-flight" price check, but for urposes of
	 * this exercise that would be unnecessarily complicated.
	 */
	@Ignore
	@Test(expected=IllegalStateException.class)
	public void testBuyWithInsufficientCash() {
		investor.setCash(4000);
		buyAndConfirm("KHC", 100, 100);
	}

	@Test(expected=IllegalStateException.class)
	public void testSellWithInsufficientShares() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 100);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100);
	}

	@Test(expected=IllegalStateException.class)
	public void testSellWithNoShares() {
		sellAndConfirm("MSFT", 1000, 100);
	}
	
	@Test
	public void testBuyPartial() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 100, 100, 80);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("KHC"), equalTo(80)));
		assertThat(investor.getCash(), closeTo(32000.0, 0.0001));
	}
	
	@Test
	public void testBuyRejected() {
		investor.setCash(40000);
		buyAndReject("KHC", 100, 100);
		assertThat(investor.getPortfolio(), not(hasKey("KHC")));
		assertThat(investor.getCash(), closeTo(40000.0, 0.0001));
	}
	
	@Test
	public void testSellPartial() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndConfirm("MSFT", 1000, 100, 500);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(9500)));
		assertThat(investor.getCash(), closeTo(50000.0, 0.0001));
	}

	@Test
	public void testSellRejected() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		investor.setPortfolio(starter);
		sellAndReject("MSFT", 1000, 100);
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(10000)));
		assertThat(investor.getCash(), closeTo(0.0, 0.0001));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidCash() {
		investor.setCash(-40000);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreateInvalidShareCount() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 0);
		investor.setPortfolio(starter);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBuyInvalidShareCount() {
		investor.setCash(40000);
		buyAndConfirm("KHC", 0, 100);
	}
}
