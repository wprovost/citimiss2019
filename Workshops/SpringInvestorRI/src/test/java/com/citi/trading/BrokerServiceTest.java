package com.citi.trading;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.pricing.Pricing;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class BrokerServiceTest {

	@Configuration
	@PropertySource("classpath:application.properties")
	public static class Config {
		
		@Bean
		public OrderPlacer mockMarket() {
			return mock(OrderPlacer.class);
		}
		
		@Bean
		public Pricing pricing() {
			return mock(Pricing.class);
		}
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			return new Investor();
		}
		
		@Bean
		public BrokerService service() {
			return new BrokerService();
		}
	}
	
	@Autowired
	private BrokerService service;
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetAll() {
		List<Investor> accounts = service.getAll();
		assertThat(accounts, contains(
			both(hasProperty("cash", closeTo(17500.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1000),
					hasEntry("AA", 500),
					hasEntry("WMT", 2000)
				))),
			both(hasProperty("cash", closeTo(21100.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1500),
					hasEntry("HON", 100),
					hasEntry("VZ", 500)
				)))
			));
	}
	
	@Test
	public void testGetByID() {
		ResponseEntity<Investor> response = service.getByID(2);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(response.getBody(), 
			both(hasProperty("cash", closeTo(21100.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1500),
					hasEntry("HON", 100),
					hasEntry("VZ", 500)
				))));
	}
	
	@Test
	public void testGetByID404() {
		ResponseEntity<Investor> response = service.getByID(3);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}
	
	@Test
	public void testOpenAccount() {
		Investor account = service.openAccount(1000);
		assertThat(account.getID(), equalTo(3));
		assertThat(account.getCash(), closeTo(1000.0, 0.0001));
		assertThat(service.getAll(), hasSize(3));
	}
	
	@Test
	public void testCloseAccount() {
		ResponseEntity<Void> response = service.closeAccount(2);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
		assertThat(service.getAll(), hasSize(1));
	}
	
	@Test
	public void testCloseAccount404() {
		ResponseEntity<Void> response = service.closeAccount(3);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}
}
