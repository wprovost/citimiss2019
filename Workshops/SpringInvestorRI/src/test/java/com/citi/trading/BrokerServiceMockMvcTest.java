package com.citi.trading;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.citi.trading.pricing.Pricing;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BrokerServiceMockMvcTest.Config.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class BrokerServiceMockMvcTest {

	@Configuration
	@EnableAutoConfiguration
	@PropertySource("classpath:application.properties")
	public static class Config {
		
		@Bean
		public OrderPlacer mockMarket() {
			return mock(OrderPlacer.class);
		}
		
		@Bean
		public Pricing pricing() {
			return mock(Pricing.class);
		}
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			return new Investor();
		}
		
		@Bean
		public BrokerService service() {
			return new BrokerService();
		}
	}
	
	@Autowired
	private BrokerService service;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private MockMvc mockMVC;
	
	/**
	Initialize the mock MVC system using an injected web-application context.
	*/
	@Before
	public void setUp () throws Exception {
		mockMVC = MockMvcBuilders.webAppContextSetup (context).build ();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetAll() throws Exception {
		MvcResult result = mockMVC.perform(get("/accounts"))
			.andExpect(status().isOk())
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andReturn();
		List<Investor> accounts = objectMapper.readValue(result.getResponse()
				.getContentAsString(), new TypeReference<List<Investor>>(){});
		assertThat(accounts, contains(
			both(hasProperty("cash", closeTo(17500.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1000),
					hasEntry("AA", 500),
					hasEntry("WMT", 2000)
				))),
			both(hasProperty("cash", closeTo(21100.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1500),
					hasEntry("HON", 100),
					hasEntry("VZ", 500)
				)))
			));
	}
	
	@Test
	public void testGetByID() throws Exception {
		MvcResult result = mockMVC.perform(get("/accounts/2"))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andReturn();
		Investor account = objectMapper.readValue(result.getResponse()
					.getContentAsString(), Investor.class);
		assertThat(account, 
			both(hasProperty("cash", closeTo(21100.0, 0.0001)))
				.and(hasProperty("portfolio", allOf(
					hasEntry("C", 1500),
					hasEntry("HON", 100),
					hasEntry("VZ", 500)
				))));
	}
	
	@Test
	public void testGetByID404() throws Exception {
		mockMVC.perform(get("/accounts/3")).andExpect(status().isNotFound());
	}
	
	@Test
	public void testOpenAccount() throws Exception {
		MvcResult result = mockMVC.perform(post("/accounts?cash=1000"))
				.andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andReturn();
		Investor account = objectMapper.readValue(result.getResponse()
					.getContentAsString(), Investor.class);
		assertThat(account.getID(), equalTo(3));
		assertThat(account.getCash(), closeTo(1000.0, 0.0001));
		assertThat(service.getAll(), hasSize(3));
	}
	
	@Test
	public void testCloseAccount() throws Exception {
		mockMVC.perform(delete("/accounts/2")).andExpect(status().isNoContent());
		assertThat(service.getAll(), hasSize(1));
	}
	
	@Test
	public void testCloseAccount404() throws Exception {
		mockMVC.perform(delete("/accounts/3")).andExpect(status().isNotFound());
	}
}
