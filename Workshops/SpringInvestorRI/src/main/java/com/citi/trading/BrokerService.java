package com.citi.trading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts")
public class BrokerService {

	@Autowired
	private ApplicationContext context;
	
	private Map<Integer,Investor> accounts = new HashMap<>();
	
	@PostConstruct
	public void init() {
		
		Map<String,Integer> portfolio1 = new HashMap<>();
		portfolio1.put("C", 1000);
		portfolio1.put("AA", 500);
		portfolio1.put("WMT", 2000);
		Investor investor1 = context.getBean(Investor.class);
		investor1.setID(1);
		investor1.setCash(17500);
		investor1.setPortfolio(portfolio1);
		accounts.put(1, investor1);
		
		Map<String,Integer> portfolio2 = new HashMap<>();
		portfolio2.put("C", 1500);
		portfolio2.put("HON", 100);
		portfolio2.put("VZ", 500);
		Investor investor2 = context.getBean(Investor.class);
		investor2.setID(2);
		investor2.setCash(21100);
		investor2.setPortfolio(portfolio2);
		accounts.put(2, investor2);
	}
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Investor> getAll() {
		List<Investor> response = new ArrayList<>();
		response.addAll(accounts.values());
		Collections.sort(response, 
				(x,y) -> Integer.compare(x.getID(), y.getID()));
		return response;
	}
	
	@GetMapping(value="{ID}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Investor> getByID(@PathVariable("ID") int ID) {
		if (accounts.containsKey(ID)) {
			return new ResponseEntity<>(accounts.get(ID), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam("cash") double cash) {
		int ID = accounts.keySet().stream().mapToInt(Integer::intValue).max().getAsInt() + 1;
		Investor investor = context.getBean(Investor.class);
		accounts.put(ID, investor);
		investor.setID(ID);
		investor.setCash(cash);
		return investor;
	}
	
	@DeleteMapping("{ID}")
	public ResponseEntity<Void> closeAccount(@PathVariable("ID") int ID) {
		if (accounts.containsKey(ID)) {
			accounts.remove(ID);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("{ID}/purchases")
	public ResponseEntity<Void> buy(@PathVariable("ID") int ID, 
			@RequestParam("symbol") String symbol, @RequestParam("size") int size) {
		if (accounts.containsKey(ID)) {
			Investor investor = accounts.get(ID);
			try {
				investor.buy(symbol, size);
				return new ResponseEntity<>(HttpStatus.CREATED);
			} catch (IllegalArgumentException ex) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PostMapping("{ID}/sales")
	public ResponseEntity<Void> sell(@PathVariable("ID") int ID, 
			@RequestParam("symbol") String symbol, @RequestParam("size") int size) {
		if (accounts.containsKey(ID)) {
			Investor investor = accounts.get(ID);
			try {
				investor.sell(symbol, size);
				return new ResponseEntity<>(HttpStatus.CREATED);
			} catch (IllegalArgumentException | IllegalStateException ex) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
