package com.citi.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Test application for the {@link Investor} system.
 * 
 * @author Will Provost
 */
@SpringBootApplication
@PropertySource("classpath:application.properties")
public class BrokerApplication {

	/**
	 * Create three investors, and for each one request a trade.
	 * Sleep a while to allow for messaging with the stock market,
	 * and then show the updated portfolio and cash balance.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BrokerApplication.class, args);
	}
}
