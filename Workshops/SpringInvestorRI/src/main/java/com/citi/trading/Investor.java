package com.citi.trading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.Pricing;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
@Component
@Scope("prototype")
public class Investor {

	private int ID;
	private Map<String,Integer> portfolio = new HashMap<>();
	private double cash;
	
	@Autowired
	private OrderPlacer market;
	
	@Autowired
	private Pricing pricing;

	/**
	 * Handler for trade confirmations.
	 */
	private class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			if (trade.getSize() != 0 && trade.getResult() != Trade.Result.REJECTED) {
				synchronized(Investor.this) {
					String stock = trade.getStock();
					if (trade.isBuy()) {
						if (!portfolio.containsKey(stock)) {
							portfolio.put(stock, 0);
						}
						portfolio.put(stock, portfolio.get(stock) + trade.getSize());
						cash -= trade.getPrice() * trade.getSize();
					} else {
						int shares = portfolio.get(stock) - trade.getSize();
						if (shares != 0) {
							portfolio.put(stock, shares);
						} else {
							portfolio.remove(stock);
						}
						cash += trade.getPrice() * trade.getSize();
					}
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	/**
	 * Accessor for the portfolio.
	 */
	public Map<String,Integer> getPortfolio() {
		return portfolio;
	}
	
	/**
	 * Mutator for portfolio contents.
	 */
	public void setPortfolio(Map<String,Integer> portfolio) {
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
	}
	
	/**
	 * Accessor for account ID. 
	 */
	public int getID() {
		return ID;
	}
	
	/**
	 * Mutator for account ID.
	 */
	public void setID(int ID) {
		this.ID = ID;
	}
	
	/**
	 * Accessor for current cash. 
	 */
	public double getCash() {
		return cash;
	}
	
	/**
	 * Mutator for cash balance.
	 */
	public void setCash(double cash) {
		if (cash < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}

		this.cash = cash;
	}
	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must buy a positive number of shares.");
		}
		
		Consumer<PriceData> subscriber = data -> {
				double price = data.getData(1).findAny().get().getClose();
				Trade trade = new Trade(stock, true, size, price);
				market.placeOrder(trade, handler);
			};
		pricing.subscribe(stock, 1, subscriber);
		pricing.getPriceData();
		pricing.unsubscribe(stock, subscriber);
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must sell a positive number of shares.");
		}
		if (!portfolio.containsKey(stock) || size > portfolio.get(stock)) {
			throw new IllegalStateException("Not enough shares to make this sale.");
		}

		Consumer<PriceData> subscriber = data -> {
				double price = data.getData(1).findAny().get().getClose();
				Trade trade = new Trade(stock, false, size, price);
				market.placeOrder(trade, handler);
			};
		pricing.subscribe(stock, 1, subscriber);
		pricing.getPriceData();
		pricing.unsubscribe(stock, subscriber);
	}
	
	@Override
	public String toString() {
		return String.format("Investor: [cash=%1.4f, portfolio=%s]", 
				cash, portfolio.toString());
	}
}
