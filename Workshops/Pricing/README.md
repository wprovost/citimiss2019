# Java Workshop

In this workshop exercise you will improve an existing component that retrieves stock pricing from a remote web service. The component works, but exhibits a number of unfortunate characteristics:

* It does very little error handling -- even of relatively likely conditions such as an HTTP 404.

* It does no logging.

* The API makes every caller who is interested in a stock to make a call each time they want an updated price point. This in turn makes the component fire off separate HTTP requests -- even for callers who want pricing for the same stock at nearly the same time.

* It has neither unit nor integration tests. The only way we're even sure that it works is by way of a **main()** method in the main component class.

## Criteria for Completed Work

Your project must be available on a public BitBucket repository, the URL of which you'll provide to the instructors for review. Implement as much of the exercise as you can, and whatever you have implemented must compile and run cleanly.

Submit your work using this [Google form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).

## Setup

Create a new Maven project in your Eclipse workspace, called Pricing. Create a new BitBucket repository and link it to the new Eclipse project. Copy all of the project files from this folder (the one this README.md lives in) into your project folder. Add, commit, and push so that your BitBucket repository is perfectly in synch with your copy of the starter code.

Run **com.citi.trading.pricing.Pricing** as a Java application. You will see that it can get current pricing: the latest price point for a single stock, including open, high, low, close, and volume for a specific timestamp.

Try the same URL that the component just used, in a browser, to get a sense of how the backing service works:

    http://will1.conygre.com:8081/prices/MRK?periods=1

You'll get a CSV-formatted response back.


## Tasks

Now, make this Java component a lot better! Make as many of the following improvements as you can in the available time. Try to keep up with adding, committing, and pushing to your repository as you go. This is not a team exercise but, still, it's good to get in the habit of regular use of git; and you get the benefit of remote backup, history, and rollback if you need it. This will also be your way of submitting work for review.

1\. The backing service can provide more than one price point. If you set the value of the **periods** parameter in the URL to, for example, 8, you'll get the most recent 8 price points, at a 15-second interval. You can get this sort of price history going back a maximum of 120 prices, or 30 minutes.

So: let's improve the component so that it can get more than one price point for its caller. Add a parameter to getPriceData so that the caller can request N periods, and change the HTTP request accordingly. You'll need a different return type for the method, as well.

Update **main()** to test this out. Commit and push when you have this working.

2\. If a lot of callers want to use this component, to request pricing on the same stock, there's a major inefficiency in sending off lots of identical HTTP requests. Also, most callers are going to call us over and over, looking for pricing updates. We could improve on both of those fronts by applying the Observer pattern, so that the **Pricing** component would register observers and could then push pricing data too all observers based on a single HTTP request.

Implement this now -- this will involve at least the following steps:

* Define an observer interface that will be implemented by each subscriber. What information will you pass in calls to this interface? Define method signature(s) accordingly.

* Make the **Pricing** class manage any number of "subscribers" who can subscribe and unsubscribe at any time. A subscriber will be interested in a specific stock, and you'll need to keep track of who wants pricing on which stock.

* Change **getPriceData()** so that it no longer takes parameters and no longer returns information directly. Instead, a call to the method will prompt the component to (a) request pricing for each stock for which there are subscribers, and (b) push that information out to all interested subscribers.

Change **main()** to test this new approach: you will need to create an implementation of your callback interface -- a subscriber -- and subscribe before calling **getPriceData** a few times. Your subscriber can for example just print out the pricing as it is received.

Remember that the web service will only have new information for you every 15 seconds; so if you want to test updates after the initial request for a stock's price, you can sleep the main thread for 15 seconds at a time.

This is another good time for git add, commit, and push.

3\. Add logging to the system. What logging, exactly? Analyze this yourself and add what you consider to be appropriate and useful.

4\. Add appropriate error handling. Consider the possibilities of bad user/caller inputs, server or network failures, and anything else that might cause trouble at runtime.

5\. Convert the **main()** method in **Pricing.java** to an integration test. Call this **PricingIntegrationTest.java**. There is not much to assert here, but at least the basic logic of connecting and getting a price point can be proved out under JUnit.

To support this you will need to update **pom.xml** with the JUnit dependency.

## Stretch Goal

The publish/subscribe system is nice, and it also puts us in a position for a further improvement. Many callers will ask for N price points and then will continue to request pricing updates. We could cache the data as we retrieve it, and then (a) hand it right over to new subscribers as their first notification, and (b) update incrementally with new prices as they come in.
