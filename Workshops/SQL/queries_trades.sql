-- 1. Find all trades by a given trader on a given stock

select case when t.buy = 1 then 'BUY' else 'SELL' end, t.size, t.price
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'
order by t.instant;


2. Find profitability for a given trader

select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1
  and t.stock = 'MRK'
  and p.closing_trade_id != p.opening_trade_id
  and p.closing_trade_id is not null;



3. Develop a view that shows profit or loss by trader

create view traders_and_profits as
select
  tr.first_name, tr.last_name,
  sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'profit'
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
  join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null
  and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name;


-- Stretch analysis

create view better as
select trd.id as trader_id, tr.buy, tr.size,
tr.instant as actual_time, tr.price as actual_price,
  p.instant as better_time, p.cls as better_price,
  p.cls - tr.price as difference,
  tr.size * case when tr.buy = 1 then p.cls - tr.price else tr.price - p.cls end as win
from trade tr
  join price_point p on tr.stock = p.stock
    and datediff(minute, p.instant, tr.instant) between -18 and 18
    and datediff(minute, p.instant, tr.instant) not between -3 and 3
  join position pos on pos.opening_trade_id = tr.id
    or pos.closing_trade_id = tr.id
  join trader trd on pos.trader_id = trd.id
where p.cls = (
    select case when tr.buy = 1 then min(p2.cls) else max(p2.cls) end
    from trade tr2
      join price_point p2 on tr2.stock = p2.stock
        and datediff(minute, p2.instant, tr2.instant) between -18 and 18
        and datediff(minute, p2.instant, tr2.instant) not between -3 and 3
        and (tr.buy = 1 and p2.cls < tr2.price or
             tr.buy = 0 and p2.cls > tr2.price)
    where tr2.id = tr.id
  );


-- Then, for example, show me better options where I would have
-- done at least 3% better:

select *
from better
where better_price / actual_price > 1.03;

-- Show total money "left on the table for trader #1:

select sum(win)
from better
where id=1;


-- Do we tend to buy/sell too early, or too late?

select count(*) as minutes
from better
where datediff(second, better_time, actual_time) > 0;


-- Is there a pattern? What are the most common lags?

select count(*), round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) * 5 as minutes
from better
group by round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) *5
order by round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) *5;

-- Cheap bar graph of those results:

select replicate('x',count(*)), round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) * 5 as minutes
from better
group by round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) *5
order by round(cast(datediff(second, better_time, actual_time) as float) / 300, 0) *5;


