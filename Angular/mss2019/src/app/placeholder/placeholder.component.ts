import { Component, OnInit } from '@angular/core';

// TODO
// set object (and array) to proper classes, so the linter is happy

@Component({
  selector: 'app-placeholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.css']
})
export class PlaceholderComponent implements OnInit {

  constructor() { }
  sourceURL:string = 'https://via.placeholder.com'
  derivedUrl:string = 'https://via.placeholder.com/150/0000FF/808080/'
  options = { sameDimensions: true, chooseColours: true, chooseFormat: true, provideText: true }
  imageParameters = { width: 240, height: 128, bgColour: '#c0ffee', fgColour: '#ff0000', format: '', text: 'placeholder' } // no format unless they specify
  urlOptions:Array<Object> = [ //
    { valueUrl: 'https://via.placeholder.com', optionText: 'placeholder' },
    { valueUrl: 'https://picsum.photos', optionText: 'general' },
    { valueUrl: 'https://placekitten.com', optionText: 'kitten' },
    { valueUrl: 'https://placebear.com', optionText: 'bear' },
    { valueUrl: 'https://placebeard.it', optionText: 'beard' },
    { valueUrl: 'http://www.fillmurray.com', optionText: 'Phil' },
    { valueUrl: 'https://www.placecage.com', optionText: 'Nicholas' },
    { valueUrl: 'https://stevensegallery.com', optionText: 'Steven' }
  ]

  ngOnInit(){

  }

  checkIfPlaceholder() {
    if (this.sourceURL == 'https://via.placeholder.com') {
      this.options = { sameDimensions: true, chooseColours: true, chooseFormat: true, provideText: true }
      this.imageParameters = { width: 240, height: 128, bgColour: '#c0ffee', fgColour: '#ff0000', format: '', text: 'placeholder' } // no format unless they specify
    }
    else {
      this.options = { sameDimensions: false, chooseColours: false, chooseFormat: false, provideText: false }
      this.imageParameters = { width: 240, height: 128, bgColour: '', fgColour: '', format: '', text: '' } // no format unless they specify
    }
  }

  resolveImage() {
    const bg = this.imageParameters.bgColour.substr(1) // strip the leading #
    const fg = this.imageParameters.fgColour.substr(1)
    // set default for placeholder.com width x height
    let dimensions = `/${this.imageParameters.width}x${this.imageParameters.height}`
    let colours = ``
    let textParam = ``
    if (this.options.sameDimensions) {
      dimensions = `/${this.imageParameters.width}`
    }
    if (this.sourceURL != 'https://via.placeholder.com/') { // we're not using placeholder.com, so use w/h
      dimensions = `/${this.imageParameters.width}/${this.imageParameters.height}`
    }
    if (this.options.chooseColours) {
      colours = `/${bg}/${fg}`
    }
    if (this.options.provideText) {
      textParam = `?text=${this.imageParameters.text}`
    }
    this.derivedUrl = `${this.sourceURL}${dimensions}${this.imageParameters.format}${colours}${textParam}`
  }
  checkDefaultFormat() {
    // reset to .png if they don't want to specify a format
    if (!this.options.chooseFormat) {
      this.imageParameters.format = ''
    }
  }
}
