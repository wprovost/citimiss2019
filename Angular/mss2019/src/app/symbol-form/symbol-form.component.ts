import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-symbol-form',
  templateUrl: './symbol-form.component.html',
  styleUrls: ['./symbol-form.component.css']
})
export class SymbolFormComponent implements OnInit {

  constructor() { }
  symbol = 'ADBE'
  qty = 50
  combined

  ngOnInit() {
    this.combined = `${this.symbol} ${this.qty}` // back-tick string builder
  }

}
