import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockParamsComponent } from './stock-params.component';

describe('StockParamsComponent', () => {
  let component: StockParamsComponent;
  let fixture: ComponentFixture<StockParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
