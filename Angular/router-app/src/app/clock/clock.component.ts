import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {

  title: any
  currentTime

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.currentTime = new Date()
    let routerObj = this.route.data.subscribe((value) => {
      this.title = value.title
    })
  }

}
