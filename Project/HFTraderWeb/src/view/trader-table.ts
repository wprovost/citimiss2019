import { Component } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate {

  service: TraderService;
  traders: Array<Trader> = [];

  /**
   * Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService) {
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    return trader.active ? "Started" : "Stopped";
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(ev: any, hardStop: boolean) {
    const index = ev.target.id.replace("trader", "");
    const trader = this.traders[index];
    this.service.setActive(trader.ID, !trader.active);
  }
}
