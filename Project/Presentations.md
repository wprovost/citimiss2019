# Tech-Fair Presentations

For tomorrow's Tech Fair, each development team will give a brief technical presentation in the morning, and a demonstration of your application in the afternoon. Following are details about each.

## Technical Presentation

Your team will give a presentation that focuses on one aspect of our project work; each team will be assigned a topic. You'll speak to an assembled audience, using a presentation machine connected to the room's main projection screens.

Consider the topic you've been assigned, and try to come up with a few cogent thoughts that you want to communicate in that technical area. Work up a PowerPoint or similar set of presentable slides. Consider the following:

* Be sure to introduce yourselves, and your names should be shown on your opening slide.

* All team members must present, for roughly equal time: shoot for about 3 minutes each.

* Plan to have one team member manage the visual presentation while another speaks.

* Speak clearly, and make eye contact.

Rehearse your presentation: with one another, in front of other teams, and with instructors. Get feedback, revise, and especially keep an eye on the clock so that you don't run long on Friday. Everything takes longer to say than you think it will take.

Be prepared to answer one or two quick questions at the end of your presentation. Remember, you're looking to demonstrate understanding of your topic; but you are not expected to be experts. If you get a challenging question, consider it, give the best honest answer you can, and don't flinch to say "we don't know."

## Demonstrating Your Applications

In the afternoon you'll have a demonstration table for your team, with one PC and two monitors. It's an open house, so you'll find yourselves demonstrating multiple times, re-starting as new people arrive, and going off on one tangent or another. You'll give at least one, end-to-end, assessed presentation during the afternoon.

You should be able to demonstrate a working application, and to show back-end behavior such as application logging, intercommunications between services, etc.

Here too you should have a PowerPoint or similar slide presentation ready, including at least the following:

* Explain the business and/or technical problem you were asked to solve.

* Identify the main challenges you faced, and describe your strategies for addressing them.

* Describe outcomes: did you solve it? Fully, partially? Are there outstanding next steps?

* Identify and describe lessons learned from the project work.

Submit all information about your completed project using this [Google Form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).