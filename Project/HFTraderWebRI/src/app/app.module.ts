import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Analysis } from '../view/analysis';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Positions } from '../view/positions';
import { TraderTable } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';

@NgModule({
  declarations: [
    Analysis,
    AppComponent,
    Positions,
    TraderTable,
    TraderToolbar
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
