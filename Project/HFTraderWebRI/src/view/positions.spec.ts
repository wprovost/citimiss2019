import { Position } from "../model/position";
import { Trade } from "../model/trade";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { Positions } from "./positions";

describe('Positions', () => {

  beforeEach(function() {
      this.mockTraderService = jasmine.createSpyObj("", [
          "subscribe",
          "unsubscribe"
        ]);

      this.mockElementRef = {
          nativeElement: {
              style: {},
              querySelector: sel => {}
            }
        };

      this.mockModalService = jasmine.createSpyObj("", [ "add", "remove" ]);

      this.positions = [
          new Position(new Trade(1, "09:00", "ABC", 100, true, 100),
              new Trade(2, "09:02", "ABC", 100, false, 103), 300, 0.03),
          new Position(new Trade(3, "09:06", "ABC", 100, false, 101), null, 0, NaN)
        ];

      this.trader =
          new TwoMovingAverages(1, "ABC", 100, true, false, this.positions,
              300, 0.03, 30000, 60000, 0.03);

      this.positionsPopup = new Positions
          (this.mockTraderService, this.mockModalService, this.mockElementRef);
      this.positionsPopup.trader = this.trader;
    });

  it("reports that the trader has some profit or loss", function() {
      expect(this.positionsPopup.traderHasProfit()).toBe(true);
    });

  it("subscribes for updates when opened", function() {
      this.positionsPopup.open();
      expect(this.mockTraderService.subscribe)
          .toHaveBeenCalledWith(this.positionsPopup);
    });

  it("unsubscribes from updates when closed", function() {
      this.positionsPopup.close();
      expect(this.mockTraderService.unsubscribe)
          .toHaveBeenCalledWith(this.positionsPopup);
    });

  it("updates its positions when it receives a trader update", function() {
      const newPositions = [
          new Position(new Trade(1, "09:00", "ABC", 100, true, 100),
              new Trade(2, "09:02", "ABC", 100, false, 103), 300, 0.03),
          new Position(new Trade(3, "09:06", "ABC", 100, false, 101),
              new Trade(4, "09:08", "ABC", 100, true, 97), 400, 0.04),
          new Position(new Trade(3, "09:11", "ABC", 100, true, 98),
              new Trade(4, "09:13", "ABC", 100, false, 94), -300, -0.033)
        ];
      const newTrader =
          new TwoMovingAverages(1, "ABC", 100, true, false, newPositions,
              300, 0.03, 30000, 60000, 0.03);

          this.positionsPopup.latestTraders([newTrader]);
          expect(this.positionsPopup.trader.positions.length).toBe(3);
    });
});
