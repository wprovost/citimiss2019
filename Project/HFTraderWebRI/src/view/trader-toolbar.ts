import { Component } from "@angular/core";
import { BollingerBands } from "../model/bollinger-bands";
import { Trader } from "../model/trader";
import { TraderService } from "../model/trader-service";
import { TwoMovingAverages } from "../model/two-moving-averages";

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"]
})
export class TraderToolbar {

  service: TraderService;
  type: string;
  stock: string;
  size: number;
  twoMALengthShort: number;
  twoMALengthLong: number;
  bbLength: number;
  bbEntryThreshold: number;
  exitThreshold: number;

  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(service: TraderService) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;
    this.twoMALengthShort = 30;
    this.twoMALengthLong = 60;
    this.bbLength = 120;
    this.bbEntryThreshold = 1.5;
    this.exitThreshold = 3;
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    let newTrader: Trader = null;
    if (this.type == "2MA") {
      newTrader = new TwoMovingAverages
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.twoMALengthShort * 1000, this.twoMALengthLong * 1000, this.exitThreshold / 100);
    } else if (this.type == "BB") {
      newTrader = new BollingerBands
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.bbLength * 1000, this.bbEntryThreshold, this.exitThreshold / 100);
    }

    this.service.createTrader(newTrader);
  }
}
