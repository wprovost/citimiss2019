import { Component } from "@angular/core";
import { BollingerBands } from "../model/bollinger-bands";
import { Trader } from "../model/trader";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { ModalService } from "./modal-service";
import { Analysis } from "./analysis";
import { Positions } from "./positions";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate {

  service: TraderService;
  modals: ModalService;
  traders: Array<Trader> = [];
  stopCommand: number = -1;

  /**
   * Helper to format times in mm:ss format.
   */
  static minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = seconds / 60;
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService, modals: ModalService) {
    this.service = service;
    service.subscribe(this);
    service.notify();

    this.modals = modals;
  }

  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
  }

  /**
   * Polymorphic description of trader's parameters.
   */
  describe(trader: Trader): string {
    if (trader instanceof TwoMovingAverages) {
      let twoMA = trader as TwoMovingAverages
      return "2MA " + (twoMA.lengthShort / 1000) +
        "/" + (twoMA.lengthLong / 1000) +
        "/" + ((twoMA.exitThreshold * 100).toFixed(2)) + "%";
    } else if (trader instanceof BollingerBands) {
      let bb = trader as BollingerBands;
      return "BB " + (bb.length / 1000) +
        "/" + (bb.entryThreshold.toFixed(1)) +
        "/" + ((bb.exitThreshold * 100).toFixed(2)) + "%";
    }

    return "(unknown type)";
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    return trader.active ? "Started" : (trader.stopping ? "Stopping" : "Stopped");
  }

  /**
   * Helper to advise if analysis is available for this type of trader.
   */
  analysisAvailableFor(trader: Trader): boolean {
    return trader instanceof TwoMovingAverages;
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }

  /**
   * Accessor for the stopCommand flag.
   */
  stopCommandGiven(index: number): boolean {
    return this.stopCommand == index;
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrGiveStopCommand(ev: any) {
    const index = ev.target.id.replace("trader", "");
    const trader = this.traders[index];
    if (trader.active) {
      this.stopCommand = index;
    } else {
      this.service.setActive(trader.ID, true, false);
    }
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  stop(ev: any, hardStop: boolean) {
    const index = ev.target.id.replace("soft", "").replace("hard", "");
    const trader = this.traders[index];
    this.stopCommand = -1;
    this.service.setActive(trader.ID, false, hardStop);
  }

  /**
   * Opens a Positions modal for the trader at the given table index.
   */
  showDetails(ev: any) {
    const index = ev.target.id.replace("details", "");
    const trader = this.traders[index];
    const positionsPopup = this.modals.open("positions") as Positions;
    positionsPopup.trader = trader;
  }

  /**
   * Opens a Positions modal for the trader at the given table index.
   */
  showAnalysis(ev: any) {
    const index = ev.target.id.replace("analysis", "");
    const trader = this.traders[index];
    const analysisPopup = this.modals.open("analysis") as Analysis;
    analysisPopup.showAnalysis(trader);
  }
}
