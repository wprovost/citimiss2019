import { Component, ElementRef } from "@angular/core";
import { Trader } from "../model/trader";
import { TraderService, TraderUpdate } from "../model/trader-service";
import { Modal } from "./modal";
import { ModalService } from "./modal-service";

/**
 * Angular component that presents the trading history of a given trader,
 * in a modal view.
 *
 * @author Will Provost
 */
@Component({
  selector: "positions",
  templateUrl: "./positions.html",
  styleUrls: ["./modal.css", "./trader-table.css"]
})
export class Positions extends Modal implements TraderUpdate {

  trader: Trader;
  traderService: TraderService;

  /**
   * Stores the injected services and the element-ref for the modal template.
   * Sets our ID for use with the modal service.
   */
  constructor(traderService: TraderService, modalService: ModalService,
      elementRef: ElementRef) {
    super(modalService, elementRef, "positions");
    this.traderService = traderService;

    this.traderHasProfit = this.traderHasProfit.bind(this);
  }

  /**
   * Helper that determines if the trader has a meaningful total profit yet.
   */
  traderHasProfit(): boolean {
    return this.trader.positions.length !== 0 &&
        this.trader.positions[0].closingTrade !== null;
  }

  /**
   * Opens the modal and subscribes for updates, so that we can add to
   * the trading history if trades occur while the user is viewing the list.
   */
  open() {
    super.open();
    this.traderService.subscribe(this);
  }

  /**
   * Close the modal view, and unsubscribe from trader updates.
   */
  close() {
    super.close();
    this.traderService.unsubscribe(this);
  }

  /**
   * Find our trader in the list, and replace our reference with the
   * new one; this will trigger a UI update in the HTML template.
   */
  latestTraders(traders: Array<Trader>) {
    const replacement = traders.filter(t => t.ID === this.trader.ID);
    if (replacement.length !== 0) {
      this.trader = replacement[0];
    }
  }
}
