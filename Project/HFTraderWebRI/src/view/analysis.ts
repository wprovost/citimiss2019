import { Component, ElementRef, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { Trader } from "../model/trader";
import { TraderService } from "../model/trader-service";
import { Modal } from "./modal";
import { ModalService } from "./modal-service";

/**
 * Angular component that presents the trading history of a given trader,
 * in a modal view.
 *
 * @author Will Provost
 */
@Component({
  selector: "analysis",
  templateUrl: "./analysis.html",
  styleUrls: ["./modal.css", "./trader-table.css"]
})
export class Analysis extends Modal {

  trader: Trader;
  traderService: TraderService;
  analysis: any;

  /**
   * Stores the injected services and the element-ref for the modal template.
   * Sets our ID for use with the modal service.
   */
  constructor(traderService: TraderService, modalService: ModalService,
      elementRef: ElementRef) {
    super(modalService, elementRef, "analysis");
    this.traderService = traderService;
    this.trader = null;
    this.analysis = null;

    this.showAnalysis = this.showAnalysis.bind(this);
  }

  /**
   *
   */
  showAnalysis(trader: Trader): void {

    this.trader = trader;
    this.traderService.getAnalysis(trader.ID)
      .then(analysis => {
          this.analysis = analysis;
          let candidates = analysis["candidates"];

          const NUMBER_TO_SHOW = 3;
          let threshold = null;
          while (candidates.length > NUMBER_TO_SHOW) {
            threshold = null;
            let index = -1;
            for (let i = 0; i < candidates.length; ++i) {
              if (!threshold || candidates[i].profitOrLoss < threshold) {
                index = i;
                threshold = candidates[i].profitOrLoss;
              }
            }
            candidates.splice(index, 1);
          }

          for (let candidate of candidates) {
            candidate.lengthShort = candidate.lengthShort / 1000;
            candidate.lengthLong = candidate.lengthLong / 1000;
            candidate.exitThreshold = candidate.exitThreshold * 100;
            candidate.color = candidate.profitOrLoss > 0
                ? "rgba(0, 192, 0, 200)"
                : "rgba(255, 0, 0, 200)";
              candidate.border = candidate.color;
          }
          candidates.push({
              lengthShort: trader["lengthShort"] / 1000,
              lengthLong: trader["lengthLong"] / 1000,
              exitThreshold: trader["exitThreshold"] * 100,
              profitOrLoss: trader.profitOrLoss,
              color: trader.profitOrLoss > 0
                ? "rgba(64, 192, 64, 200)"
                : "rgba(255, 64, 64, 200)",
              border: "rgba(64, 64, 64, 200)"
            });

          let maxProfit = null;
          let minX = null;
          let maxX = null;
          let minY = null;
          let maxY = null;
          for (let candidate of candidates) {
            if (!maxProfit || Math.abs(candidate.profitOrLoss) > maxProfit) {
              maxProfit = Math.abs(candidate.profitOrLoss);
            }
            if (!minX || candidate.lengthLong < minX) {
              minX = candidate.lengthLong;
            }
            if (!maxX || candidate.lengthLong > maxX) {
              maxX = candidate.lengthLong;
            }
            if (!minY || candidate.exitThreshold < minY) {
              minY = candidate.exitThreshold;
            }
            if (!maxY || candidate.exitThreshold > maxY) {
              maxY = candidate.exitThreshold;
            }
          }
          let radiusScale = 15 / maxProfit;
          minX -= 15;
          maxX += 15;
          minY -= .25;
          maxY += .25;

          let colors = [];
          let borders = [];
          let data = [];
          for (let candidate of candidates) {
            colors.push(candidate.color);
            borders.push(candidate.border);
            data.push({ x: candidate.lengthLong, y: candidate.exitThreshold, r: radiusScale * Math.abs(candidate.profitOrLoss) });
          }

          let container = document.getElementById("container");
          let definition = {
              type: "bubble",
              data: {
                  datasets: [{
                      backgroundColor: colors,
                      borderColor: borders,
                      data: data
                    }]
                },
              options: {
                  legend: { display: false },
                  scales: {
                      xAxes: [{
                          display: true,
                          scaleLabel: {
                              display: true,
                              labelString: "Long window (seconds)"
                            },
                          ticks: {
                              suggestedMin: minX,
                              suggestedMax: maxX,
                              stepSize: 15
                            }
                        }],
                      yAxes: [{
                          display: true,
                          scaleLabel: {
                              display: true,
                              labelString: "Exit threshold (%)"
                            },
                          ticks: {
                              suggestedMin: minY,
                              suggestedMax: maxY,
                              stepSize: .25,
                              callback: (value, index, values) => "" + value.toFixed(2)
                            }
                        }],
                      responsive:true,
                      maintainAspectRatio: false
                    },
                  tooltips: {
                      callbacks: {
                          label: (tip, data) => "Short " + candidates[tip.index].lengthShort +
                              " Long " + candidates[tip.index].lengthLong +
                              " Exit " + candidates[tip.index].exitThreshold +
                              "% Profit $" + candidates[tip.index].profitOrLoss.toFixed(2)
                        }
                    }
                }
            };
          let chart = new Chart(document.getElementById("chart"), definition);
        });
  }
}
