package com.citi.trading.order;

import java.security.SecureRandom;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.TextMessage;

import com.citi.trading.Trade;
import com.citi.trading.jms.PointToPointClient;
import com.citi.trading.mq.TradeMessenger;

public class OrderBroker
    implements MessageListener
{
    private static Logger LOGGER = 
        Logger.getLogger (OrderBroker.class.getName ());

    private static final SecureRandom randomGenerator = new SecureRandom ();
    public static Supplier<Integer> outcomeGenerator = 
            () -> (int) (randomGenerator.nextDouble() * 100);
    public static Supplier<Double> delayFactorGenerator = 
            () -> randomGenerator.nextDouble ();
    
    private Timer timer = new Timer ();
    
    public void sendTradeResponse (String requestID, Queue responseQueue, 
        Trade trade, Trade.Result result, int shares)
    {
        trade.setResult (result);
        trade.setSize (shares);
        trade.setToNow ();

        double factor = delayFactorGenerator.get();
        int delayInMSec = (int) (factor * factor * factor * 10000) + 500;
            
        class DelayedResponse
            extends TimerTask
        {
            private String requestID;
            private Queue responseQueue;
            private Trade trade;
            
            public DelayedResponse (String requestID, Queue responseQueue, Trade trade)
            {
                this.trade = trade;
                this.requestID = requestID;
                this.responseQueue = responseQueue;
            }
            
            @Override
            public void run ()
            {
                try ( PointToPointClient responseClient = 
                    new PointToPointClient(responseQueue); )
                {
                    QueueSender sender = responseClient.openToSend ();
                    Message reply = responseClient.getSession ().createTextMessage 
                        (TradeMessenger.tradeToXML (trade));
                    reply.setJMSCorrelationID (requestID);
                    sender.send (reply);
                 
                    LOGGER.info("Responded " + result + " to queue: " + responseQueue +
                    		" correlation ID " + requestID);
                }
                catch (Exception ex)
                {
                    LOGGER.log (Level.SEVERE, 
                        "Failed to send response to confirm requested trade", ex);
                }
            }
        }
        timer.schedule (new DelayedResponse (requestID, responseQueue, trade), delayInMSec);
    }
    
    /**
    Take incoming trade messages, break out the Trade content,
    and {@link #makeTrade place the trade with the OrderManager}.
    */
    @Override
    public void onMessage (Message message)
    {
        try
        {
            LOGGER.log (Level.INFO, "Received trade request: " + message.getJMSCorrelationID ());
            
            if (!(message instanceof TextMessage))
            {
                LOGGER.log (Level.WARNING, 
                    "Received non-text message -- ignoring.");
                return;
            }
            
            Trade trade = TradeMessenger.parseTradeMessage ((TextMessage) message);
            
            String requestID = message.getJMSCorrelationID ();
            if (requestID == null)
            {
                LOGGER.log (Level.WARNING, 
                    "No correlation ID found in trade request; ignoring.");
                return;
            }
            
            Queue responseQueue = (Queue) message.getJMSReplyTo ();
            if (responseQueue == null)
            {
                LOGGER.log (Level.WARNING, 
                    "No response queue found in trade request; ignoring.");
                return;
            }
            
            int outcome = outcomeGenerator.get ();
            if (outcome < 2)
            {
                LOGGER.log (Level.WARNING, 
                    "Sorry ... just too busy, not going to respond to " + requestID + 
                    " from " + responseQueue.getQueueName() + ".");
                return;
            }

            int shares = trade.getSize();
            Trade.Result result = Trade.Result.FILLED;
            if (outcome < 5)
            {
                shares = 0;
                result = Trade.Result.REJECTED;
            }
            else if (outcome < 25)
            {
                shares *= (outcome / 5) * .20;
                result = Trade.Result.PARTIALLY_FILLED;
            }
                
            sendTradeResponse (requestID, responseQueue, trade, result, shares);
        }
        catch (Exception ex)
        {
            LOGGER.log (Level.SEVERE, "Unable to process trade request", ex);
        }
    }
    
    public static void main (String[] args)
    {
    	for (String arg : args) {
    		if (arg.equalsIgnoreCase("alwaysfill")) {
    			System.out.println("As requested, I will fill all orders -- no partials rejections, or no-answers.");
    			outcomeGenerator = () -> 100;
    		}
    		if (arg.equalsIgnoreCase("nodelay")) {
    			System.out.println("As requested, I will reply (almost) immediately -- no artificial delays.");
    			delayFactorGenerator = () -> 0.0;
    		}
    	}
    	
        try ( TradeMessenger service = new TradeMessenger (); )
        {
            QueueReceiver receiver = service.openToReceive ();
            receiver.setMessageListener (new OrderBroker ());
            System.out.println ("Listening for trade messages on queue: " +
            		service.getQueue().getQueueName());
            System.out.println ("Hit ENTER to quit ...");
            System.in.read ();
        }
        catch (Exception ex)
        {
            LOGGER.log (Level.SEVERE, "OrderBroker crashed!", ex);
        }
    }
}
