package com.citi.trading.analytics;

import static com.citi.trading.analytics.TwoMovingAveragesAnalysisTest.createPrices;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricePointRepository;
import com.citi.trading.strategy.TwoMovingAverages;

/**
 * Unit test for the {@link Analyst} component.
 * Each test case whips up some pricing data, stuffs it in the in-memory
 * database, instantiates a target trader, and requests an analysis.
 * It then checks for expected "winners."
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=AnalystTest.Config.class)
public class AnalystTest {

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=Trade.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
		
		@Bean
		public Analyst analyst() {
			return new Analyst();
		}
	}
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	private PricePointRepository pricePointRepository;
	
	@Autowired
	private Analyst analyst;
	
	public static final String STOCK = "AA";
	public static final int SIZE = 1000;
	
	@Before
	public void setUp() throws Exception {
		testDB.reset();
	}
	
	@Test
	public void testAnalysis123321() {
		
		List<PricePoint> prices = createPrices(1, 2, 3, 3, 2, 1, 1, 2, 3, 3, 2, 1,
				1, 2, 3, 3, 2, 1, 1, 2, 3, 3, 2, 1, 1);
		pricePointRepository.saveAll(prices);
		
		TwoMovingAverages twoMA = new TwoMovingAverages(STOCK, SIZE, 30000, 60000, .03);
		twoMA.setStartedTrading(prices.get(6).getTimestamp());
		twoMA.setStoppedTrading(prices.get(24).getTimestamp());
		
		Analysis<TwoMovingAverages> analysis = analyst.analyze(twoMA);
		assertThat(analysis.getWinner().getProfitOrLoss(), closeTo(5000.0, 0.0001));
	}
	
	@Test
	public void testAnalysisSineWave() {
		
		List<PricePoint> prices = createPrices(
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505);
		pricePointRepository.saveAll(prices);

		TwoMovingAverages twoMA = new TwoMovingAverages(STOCK, SIZE, 30000, 60000, .03);
		twoMA.setStartedTrading(prices.get(8).getTimestamp());
		twoMA.setStoppedTrading(prices.get(79).getTimestamp());
		
		Analysis<TwoMovingAverages> analysis = analyst.analyze(twoMA);
		assertThat(analysis.getWinner().getProfitOrLoss(), closeTo(26488.80, 0.0001));
	}
}
