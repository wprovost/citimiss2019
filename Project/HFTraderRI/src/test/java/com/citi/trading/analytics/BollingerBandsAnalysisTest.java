package com.citi.trading.analytics;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.strategy.BollingerBands;

/**
 * Unit test for the {@link BollingerBandsAnalysis} class.
 *
 * @author Will Provost
 */
public class BollingerBandsAnalysisTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 1000;
	
	/**
	 * Helper to create a complete list of {@link PricePoint}s given a
	 * series of closing prices. 
	 */
	public static List<PricePoint> createPrices(double... closingPrices) {
		long time = System.currentTimeMillis() - closingPrices.length * 15000;
		List<PricePoint> result = new ArrayList<>();
		for (double price : closingPrices) {
			PricePoint point = new PricePoint(new Timestamp(time), 
					price, price, price, price, 10000);
			point.setStock(STOCK);
			result.add(point);
			
			time += 15000;
		}
		return result;
	}
	
	/**
	 * Saves a perfect sine wave of pricing data, and creates a target strategy
	 * that will do okay on that pricing curve. Runs the ananlysis and checks
	 * that a better alternative is found.
	 */
	@Test
	public void testAnalysisSineWave() {
		
		List<PricePoint> prices = createPrices(
				61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,62.59,
				63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,62.59,
				61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,62.59,
				63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,62.59,
				61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,62.59,
				63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,62.59,
				61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,62.59,
				63.7505,64.7974,65.6282,66.1616);

		BollingerBands onePointNine = new BollingerBands(STOCK, SIZE, 180000, 1.9, .03);
		onePointNine.setStartedTrading(prices.get(19).getTimestamp());
		onePointNine.setStoppedTrading(prices.get(73).getTimestamp());
		
		BollingerBandsAnalysis analysis = new BollingerBandsAnalysis(onePointNine, prices);
		BollingerBands winner = analysis.run();
		
		assertThat(winner.getProfitOrLoss(), closeTo(20993.50, 0.0001));
	}
}
