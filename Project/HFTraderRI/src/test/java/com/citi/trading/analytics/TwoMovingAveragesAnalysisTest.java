package com.citi.trading.analytics;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.strategy.TwoMovingAverages;

/**
 * 
 *
 * @author Will Provost
 */
public class TwoMovingAveragesAnalysisTest {

	public static final String STOCK = "AA";
	public static final int SIZE = 1000;
	
	/**
	 * Helper to create a complete list of {@link PricePoint}s given a
	 * series of closing prices. 
	 */
	public static List<PricePoint> createPrices(double... closingPrices) {
		long time = System.currentTimeMillis() - closingPrices.length * 15000;
		List<PricePoint> result = new ArrayList<>();
		for (double price : closingPrices) {
			PricePoint point = new PricePoint(new Timestamp(time), 
					price, price, price, price, 10000);
			point.setStock(STOCK);
			result.add(point);
			
			time += 15000;
		}
		return result;
	}
	
	/**
	 * Saves a sawtooth wave of pricing data, and creates a target strategy
	 * that will do okay on that pricing curve. Runs the ananlysis and checks
	 * that a better alternative is found.
	 */
	@Test
	public void testAnalysis123321() {
		
		List<PricePoint> prices = createPrices(1, 2, 3, 3, 2, 1, 1, 2, 3, 3, 2, 1,
				1, 2, 3, 3, 2, 1, 1, 2, 3, 3, 2, 1, 1);

		TwoMovingAverages twoMA = new TwoMovingAverages(STOCK, SIZE, 30000, 60000, .03);
		twoMA.setStartedTrading(prices.get(6).getTimestamp());
		twoMA.setStoppedTrading(prices.get(24).getTimestamp());
		
		TwoMovingAveragesAnalysis analysis = new TwoMovingAveragesAnalysis(twoMA, prices);
		TwoMovingAverages winner = analysis.run();
		
		List<TwoMovingAverages> candidates = analysis.getCandidates();
		for (TwoMovingAverages strat : candidates) {
			System.out.format("%d %d %1.4f profit of %,9.2f on %d positions%n", 
					strat.getLengthShort(), strat.getLengthLong(), 
					strat.getExitThreshold(), strat.getProfitOrLoss(),
					strat.getPositions().size());
		}

		assertThat(winner.getProfitOrLoss(), closeTo(5000.0, 0.0001));
	}
	
	/**
	 * Saves a perfect sine wave of pricing data, and creates a target strategy
	 * that will do okay on that pricing curve. Runs the ananlysis and checks
	 * that a better alternative is found.
	 */
	@Test
	public void testAnalysisSineWave() {
		
		List<PricePoint> prices = createPrices(
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505,
				62.59,61.4295,60.3826,59.5518,59.0184,58.8346,59.0184,59.5518,60.3826,61.4295,
				62.59,63.7505,64.7974,65.6282,66.1616,66.3454,66.1616,65.6282,64.7974,63.7505);

		TwoMovingAverages twoMA = new TwoMovingAverages(STOCK, SIZE, 30000, 60000, .03);
		twoMA.setStartedTrading(prices.get(8).getTimestamp());
		twoMA.setStoppedTrading(prices.get(79).getTimestamp());
		
		TwoMovingAveragesAnalysis analysis = new TwoMovingAveragesAnalysis(twoMA, prices);
		TwoMovingAverages winner = analysis.run();
		assertThat(winner.getProfitOrLoss(), closeTo(26488.80, 0.0001));
	}
}
