package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the Bollinger Bands trading strategy.
 * We check the latest closing price's deviation from the rolling mean
 * against a multiple of the rolling standard deviation. When it leaves
 * the band, we play for it to regress back into band: shorting if it
 * has run out high, and buying (going long) if it runs out low.
 * Once open, we close on a configurable percentage gain or loss.
 *
 * @author Will Provost
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {
	
    private static final Logger LOGGER =
        Logger.getLogger(BollingerBandsTrader.class.getName ());
    
    private boolean isInside = true;
    
    public BollingerBandsTrader(PricingSource pricing, 
    		OrderPlacer market, StrategyPersistence strategyPersistence) {
    	super(pricing, market, strategyPersistence);
    }
    
    protected int periods(int msec) {
    	return msec / 1000 / SECONDS_PER_PERIOD;
    }
    
    public int getNumberOfPeriodsToWatch() {
    	return periods(strategy.getLength());
    }
    
    private void checkDeviations(PriceData data) {
    	int window = periods(strategy.getLength());
    	double mean = data.getWindowAverage(window, PricePoint::getClose);
    	double meanDeviation = data.getWindowAverage(window, 
    			p -> Math.pow(p.getClose() - mean, 2));
    	double standardDeviation = Math.sqrt(meanDeviation);
    	double latestClose = data.getData(1).findAny().get().getClose();
    	isInside = Math.abs(latestClose - mean) <= 
    			standardDeviation * strategy.getEntryThreshold();

    	LOGGER.fine(String.format("Trader %d close=%1.4f mean=%1.4f stdev=%1.4f",
    			strategy.getId(), latestClose, mean, standardDeviation));
    }
    
    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
    protected void handleDataWhenOpen(PriceData data) { 
    	checkDeviations(data);
    	double currentPrice = data.getData(1).findAny().get().getClose();
    	double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    	double profitOrLoss = currentPrice / openingPrice - 1.0;
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
    }
    
    /**
     * When we're closed, check the actual deviation from mean of the closing price,
     * and see if it is more than the configured multiple of the rolling
     * standard deviation. If so, open a position aiming to profit when the
     * price falls back into the band.
     */
    protected void handleDataWhenClosed(PriceData data) {
    	if (tracking.get()) {
	    	boolean wasInside = isInside;
	    	checkDeviations(data);
	    			
			if (wasInside && !isInside) {
		    	double latestClose = data.getData(1).findAny().get().getClose();
		    	double mean = data.getWindowAverage
		    			(periods(getStrategy().getLength()), PricePoint::getClose);
				opener.placeOrder(latestClose < mean, latestClose);
				LOGGER.info("Trader " + strategy.getId() + " taking a " +
						(latestClose > mean ? "short" : "long") +
						" position as latest closing price left the the band.");
			}
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	checkDeviations(data);
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
    }
}
