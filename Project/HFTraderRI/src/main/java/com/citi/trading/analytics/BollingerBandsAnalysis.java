package com.citi.trading.analytics;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.strategy.BollingerBands;
import com.citi.trading.strategy.BollingerBandsTrader;

/**
 * Analysis for BB strategies.
 *
 * @author Will Provost
 */
public class BollingerBandsAnalysis extends Analysis<BollingerBands> {

	private static final Logger LOGGER = 
			Logger.getLogger(BollingerBandsAnalysis.class.getName());

	public BollingerBandsAnalysis(BollingerBands target, List<PricePoint> prices) {
		super(target, prices);
	}

	/**
	 * Creates a three-dimensional matrix of candidates:
	 * <ul>
	 *   <li>Either three (if the target is very short to start with) 
	 *   	or five (if there's more room) options for length 
	 *   <li>Five options for the entry threshold</li>
	 *   <li>Five options for the exit threshold</li>
	 * </ul>
	 */
	protected List<BollingerBandsTrader> createCandidates() {
		int periods = target.getLength() / Pricing.SECONDS_PER_PERIOD / 1000;
		double entryThreshold = target.getEntryThreshold();
		double exitThreshold = target.getExitThreshold();

		List<Integer> range = new ArrayList<>();
		if (periods < 4) {
			if (periods > 1) {
				range.add(periods - 1);
			}
			range.add(periods);
			range.add(periods + 1);
		} else if (periods < 8) {
			range.add(periods / 2);
			range.add(periods);
			range.add(periods * 3 / 2);
		} else {
			range.add(periods * 2 / 4);
			range.add(periods * 3 / 4);
			range.add(periods);
			range.add(periods * 5 / 4);
			range.add(periods * 6 / 4);
		}
		
		LOGGER.info("Bracketing window length over " + 
				range.stream().map(n -> "" + n)
					.collect(Collectors.joining(", ", "[", "]")) + 
				" periods.");
		LOGGER.info(String.format("Bracketing entry threshold from %1.4f to %1.4f " +
				"with an increment of %1.4f.", entryThreshold - .4, exitThreshold + .4, .2));
		LOGGER.info(String.format("Bracketing exit threshold from %1.4f to %1.4f " +
				"with an increment of %1.4f.", exitThreshold / 2, exitThreshold * 2, exitThreshold / 4));
		
		List<BollingerBandsTrader> result = new ArrayList<>();
		int ID = 0;
		for (int periodsCandidate : range) {
			for (int entryStep = -2; entryStep <= 2; ++entryStep) {
				for (int numerator = 2; numerator <= 8; ++numerator) {
					BollingerBands candidate = new BollingerBands
						(target.getStock(), target.getSize(), 
							periodsCandidate * Pricing.SECONDS_PER_PERIOD * 1000, 
							entryThreshold + entryStep * .2, 
							exitThreshold * numerator / 4);
					candidate.setId(++ID);
					BollingerBandsTrader trader = new BollingerBandsTrader
							(pricing, market, persistence);
					trader.setStrategy(candidate);
					result.add(trader);
				}
			}
		}
		
		return result;
	}
}
