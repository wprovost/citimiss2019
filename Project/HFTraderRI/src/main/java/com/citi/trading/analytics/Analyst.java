package com.citi.trading.analytics;

import java.lang.reflect.Constructor;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricePointRepository;
import com.citi.trading.strategy.BollingerBands;
import com.citi.trading.strategy.Strategy;
import com.citi.trading.strategy.TwoMovingAverages;

/**
 * Component that will perform analyses on given strategies.
 *
 * @author Will Provost
 */
@Component
public class Analyst {

	private Map<Class<? extends Strategy>, Class<? extends Analysis<?>>> 
		strategyToAnalysis = new HashMap<>();

	@Autowired
	private PricePointRepository pricePointRepository;
	
	public Analyst() {
		strategyToAnalysis.put(TwoMovingAverages.class, TwoMovingAveragesAnalysis.class);
		strategyToAnalysis.put(BollingerBands.class, BollingerBandsAnalysis.class);
	}
	
	/**
	 * Gets the stored pricing history for a period of time that covers the most
	 * recent trading period for the given trader -- and a little extra on the
	 * early side, in case alternative strategies need it. Determines the 
	 * appropriate {@link Analysis} subtype for the given strategy, 
	 * instantiates it with the target strategy and the fetched pricing,
	 * kicks it off, and returns it to the caller.
	 * 
	 * @return A completed analysis of the target trader (i.e. no need to run() it yourself).
	 */
	public <S extends Strategy> Analysis<S> analyze(S target) {

		Timestamp startingPoint = new Timestamp
				(target.getStartedTrading().getTime() - 600000); // just a little headroom
		List<PricePoint> prices = pricePointRepository.findByStockAndRange
				(target.getStock(), startingPoint, target.getStoppedTrading());
		System.out.println("Retrieved " + prices.size());
		
		try {
			Class<?> cls = strategyToAnalysis.get(target.getClass());
			Constructor<?> ctor = cls.getConstructor(target.getClass(), List.class);
			
			@SuppressWarnings("unchecked")
			Analysis<S> analysis = (Analysis<S>) ctor.newInstance(target, prices);
			
			analysis.run();
			return analysis;
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't instantiate analysis for strategy of type " +
					target.getClass().getName(), ex);
		}
	}
}
