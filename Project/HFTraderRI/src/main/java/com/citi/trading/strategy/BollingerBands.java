package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a Bollinger-Bands trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Will Provost
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBands extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;

	private int length;
	private double entryThreshold;
	private double exitThreshold;

	public BollingerBands() {
	}
	
	public BollingerBands(String stock, int size, int length, 
			double entryThreshold, double exitThreshold) {
		super(stock, size);
		this.length = length;
		this.entryThreshold = entryThreshold;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	public double getEntryThreshold() {
		return this.entryThreshold;
	}

	public void setEntryThreshold(double threshold) {
		this.entryThreshold = threshold;
	}

	public int getLength() {
		return this.length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return String.format("BollingerBands: [length=%d, entry=%1.4f, exit=%1.4f, %s]",
				length, entryThreshold, exitThreshold, stringRepresentation());
	}
}
