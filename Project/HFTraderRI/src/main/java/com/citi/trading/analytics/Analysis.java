package com.citi.trading.analytics;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.Position;
import com.citi.trading.strategy.Strategy;
import com.citi.trading.strategy.StrategyPersistence;
import com.citi.trading.strategy.Trader;

/**
 * <p>Base class for analysis of the performance of a trading strategy.
 * An analysis will look over the stored pricing data for the range of time
 * that the "target" strategy most recently traded. It will run the target
 * and some list of hypothetical alternatives against that pricing history,
 * with fake pricing supply (so as to speed up the process and not wait 
 * 15 seconds for each new price point), a fake market (which always fills
 * all requested trades immediately), and fake persistence (so that none of
 * this trading activity is ever recorded).</p>
 *
 * <p>The full trading record and total profitability of the various 
 * alternatives is then available in the strategy objects themselves; 
 * the analysis also returns the most profitable alternative as the "winner."</p>
 * 
 * <p>Derived classes must determine how to spin up a list of alternative strategies:
 * naturally, this logic will vary by the strategy type.</p>
 *
 * @author Will Provost
 */
public abstract class Analysis<S extends Strategy> {
	
	/**
	 * The fake pricing source does nothing; actual price pushes happen in
	 * our analysis algorithm.
	 */
	public static class FakePricingSource implements PricingSource {
		public void subscribe(String symbol, int capacity, Consumer<PriceData> subscriber) {}
		public void unsubscribe(String symbol, Consumer<PriceData> subscriber) {}
	}
	
	/**
	 * The fake market stores off the reqeusted trade and trader.
	 * It does not confirm the trade, because that would result in a confusing
	 * timeline for the trader in which the notification would come before the
	 * trader was finished sending the request! Instead the analysis algorithm
	 * performs the notification.
	 */
	public static class FakeOrderPlacer implements OrderPlacer {
		
		public Trade latestTrade;
		public Consumer<Trade> latestTrader;
		
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			latestTrade = order;
			latestTrader = callback;
		}

		public void fillLatestOrder() {
			if (latestTrader != null) {
				latestTrade.setResult(Trade.Result.FILLED);
				latestTrader.accept(latestTrade);
				latestTrade = null;
				latestTrader = null;
			}
		}
	}
	
	/**
	 * The fake persistence service performs the same validation as the real one,
	 * which may be especially useful when we're driving tons of trades through
	 * tons of hypothetical and possibly misbegotten strategies; and it returns
	 * the bare minimum of in-memory objects (positions and trades) expected
	 * by the traders. 
	 */
	public static class FakeStrategyPersistence implements StrategyPersistence {
		
		public Position open(Strategy strategy, Trade openingTrade) {
			if (strategy.getOpenPosition() != null) {
				throw new IllegalStateException
					("Strategy " + strategy.getId() + " already has an open position.");
			}

			Position position = new Position(strategy, openingTrade);
			strategy.addPosition(position);
			return position;
		}
		
		public Position close(Strategy strategy, Trade closingTrade) {
			Position position = strategy.getOpenPosition();
			if (position == null) {
				throw new IllegalStateException
					("Strategy " + strategy.getId() + " has no open position that could be closed.");
			}
			
			Trade openingTrade = position.getOpeningTrade();
			if (!openingTrade.getStock().equals(closingTrade.getStock()) ||
					openingTrade.isBuy() == closingTrade.isBuy() ||
					openingTrade.getSize() != closingTrade.getSize()) {
				throw new IllegalArgumentException
					(String.format("For strategy " + strategy.getId() + ", the closing trade is incompatible with the opening trade: (%s, %s, %d) vs. ((%s, %s, %d).",
						openingTrade.getStock(), "" + openingTrade.isBuy(), openingTrade.getSize(),
						closingTrade.getStock(), "" + closingTrade.isBuy(), closingTrade.getSize()));
			}
			
			position.setClosingTrade(closingTrade);
			return position;
		}
		
		public Position splitAndClosePart(Strategy strategy, Trade closingTrade) {
			Position position = strategy.getOpenPosition();
			if (position == null) {
				throw new IllegalStateException
					("Strategy " + strategy.getId() + " has no open position that could be closed.");
			}
			
			Trade openingTrade = position.getOpeningTrade();
			if (!openingTrade.getStock().equals(closingTrade.getStock()) ||
					openingTrade.isBuy() == closingTrade.isBuy() ||
					openingTrade.getSize() <= closingTrade.getSize() ||
					closingTrade.getSize() == 0) {
				throw new IllegalArgumentException
					(String.format("For strategy " + strategy.getId() + ", the closing trade is incompatible with the opening trade: (%s, %s, %d) vs. ((%s, %s, %d).",
						openingTrade.getStock(), "" + openingTrade.isBuy(), openingTrade.getSize(),
						closingTrade.getStock(), "" + closingTrade.isBuy(), closingTrade.getSize()));
			}
			
			int leftOpen = openingTrade.getSize() - closingTrade.getSize();
			openingTrade.setSize(closingTrade.getSize());
			position.setOpeningTrade(openingTrade);
			
			position.setClosingTrade(openingTrade);
			
			Trade newOpeningTrade = new Trade(openingTrade.getWhen(), openingTrade.getStock(), 
					openingTrade.isBuy(), leftOpen, openingTrade.getPrice());
			return open(strategy, newOpeningTrade);
		}
		public Strategy startTrading(Strategy strategy) {
			return strategy;
		}
		public Strategy stopTrading(Strategy strategy) {
			return strategy;
		}
	}
	
	protected S target;
	protected List<PricePoint> prices;
	protected int initialPeriodsAvailable;
	protected FakePricingSource pricing;
	protected FakeOrderPlacer market;
	protected FakeStrategyPersistence persistence;
	protected List<? extends Trader<S>> candidates;
	protected S winner;
	
	/**
	 * Provide the target for analysis, and pricing history sufficient to
	 * re-run that trader's recent activity. It's also good to provide some
	 * "headroom" in the form of earlier pricing than the target strategy 
	 * needed; that way, it's possible to test alternatives that need to 
	 * dig a little further back from the starting time (e.g. a 2MA trader
	 * with a longer long-average window). 
	 */
	public Analysis(S target, List<PricePoint> prices) {
		
		this.target = target;
		this.prices = prices;
		Timestamp start = target.getStartedTrading();
		initialPeriodsAvailable = (int) prices.stream().map(PricePoint::getTimestamp)
				.filter(p -> !p.after(start)).count();

		pricing = new FakePricingSource();
		market = new FakeOrderPlacer();
		persistence = new FakeStrategyPersistence();
	}
	
	/**
	 * Derived classes implement this to provide a list of alternative 
	 * strategies.
	 */
	protected abstract List<? extends Trader<S>> createCandidates();
	
	/**
	 * After some initial validation, we feed pricing data to all of the 
	 * alternative strategies, in parallel, and let them trade in the sandbox
	 * that we've set up for them. Then we find the strategy with the highest
	 * profit, and return it as the "winner" -- with no attempt made to select
	 * between multiple strategies that all got the maximum profit.
	 * 
	 *  The full range of {@link #getCandidates() candidates} as well as 
	 *  the determined {@link #getWinner() winner} are available after
	 *  the analysis is complete.
	 */
	public S run() {
		
		candidates = createCandidates();
		if (candidates.isEmpty()) {
			throw new IllegalStateException("No traders to compare!");
		}

			if (candidates.stream().map(Trader::getStrategy)
				.map(s -> s.getStock() + s.getSize() + 
						s.getStartedTrading() + s.getStoppedTrading())
				.distinct().count() != 1) {
			throw new IllegalStateException("All of the traders to be compared " +
					"must have exactly the same stock, size, starting and stopping times.");
		}
		
		int periodsToWatch = candidates.stream()
				.mapToInt(Trader::getNumberOfPeriodsToWatch).max().getAsInt();
		int endingPoint = (int) prices.stream().map(PricePoint::getTimestamp)
				.filter(p -> !p.after(target.getStoppedTrading())).count();
		if (periodsToWatch > initialPeriodsAvailable) {
			throw new IllegalStateException
					("Some traders need more data at start than we have available.");
		}
		
		PriceData data = new PriceData(target.getStock(), periodsToWatch);
		for (int index = 0; index < endingPoint; ++index) {
			data.addData(prices.get(index));
			if (index > initialPeriodsAvailable) {
				for (Trader<S> candidate : candidates) {
					candidate.accept(data);
					market.fillLatestOrder();
				}
			}
		}
		
		double bestProfit = candidates.stream().map(Trader::getStrategy)
				.mapToDouble(Strategy::getProfitOrLoss).max().getAsDouble();
		return winner = candidates.stream().map(Trader::getStrategy)
				.filter(s -> s.getProfitOrLoss() == bestProfit).findAny().get();
	}

	/**
	 * Get all of the candidates and their trading histories and profitability.
	 * 
	 * @throws IllegalStateException If you call this before running the analysis 
	 */
	public List<S> getCandidates() {
		if (winner == null) {
			throw new IllegalStateException("Haven't run the analysis yet.");
		}

		return Collections.unmodifiableList(candidates.stream()
				.map(Trader::getStrategy).collect(Collectors.toList()));
	}
	
	/**
	 * Get the most profitable strategy.
	 * 
	 * @throws IllegalStateException If you call this before running the analysis 
	 */
	public S getWinner() {
		if (winner == null) {
			throw new IllegalStateException("Haven't run the analysis yet.");
		}

		return winner;	
	}
}
