package com.citi.trading.analytics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.strategy.TwoMovingAverages;
import com.citi.trading.strategy.TwoMovingAveragesTrader;

/**
 * Analysis for 2MA strategies.
 *
 * @author Will Provost
 */
public class TwoMovingAveragesAnalysis extends Analysis<TwoMovingAverages> {

	private static final Logger LOGGER = 
			Logger.getLogger(TwoMovingAveragesAnalysis.class.getName());

	public TwoMovingAveragesAnalysis(TwoMovingAverages target, List<PricePoint> prices) {
		super(target, prices);
	}

	/**
	 * Creates a three-dimensional matrix of candidates:
	 * <ul>
	 *   <li>Either three (if the target is very short to start with) 
	 *   	or five (if there's more room) options for short average 
	 *   <li>Five options for the long range -- but then weeding out
	 *   	any options equal to or less than the associated short range,
	 *   	and any that would have us requesting more pricing data
	 *   	than was recorded</li>
	 *   <li>Five options for the exit threshold</li>
	 * </ul>
	 */
	protected List<TwoMovingAveragesTrader> createCandidates() {
		int shortPeriods = target.getLengthShort() / Pricing.SECONDS_PER_PERIOD / 1000;
		int longPeriods = target.getLengthLong() / Pricing.SECONDS_PER_PERIOD / 1000;
		double threshold = target.getExitThreshold();
		
		List<Integer> shortRange = new ArrayList<>();
		if (shortPeriods < 4) {
			if (shortPeriods > 1) {
				shortRange.add(shortPeriods - 1);
			}
			shortRange.add(shortPeriods);
			shortRange.add(shortPeriods + 1);
		} else if (shortPeriods < 8) {
			shortRange.add(shortPeriods / 2);
			shortRange.add(shortPeriods);
			shortRange.add(shortPeriods * 3 / 2);
		} else {
			shortRange.add(shortPeriods * 2 / 4);
			shortRange.add(shortPeriods * 3 / 4);
			shortRange.add(shortPeriods);
			shortRange.add(shortPeriods * 5 / 4);
			shortRange.add(shortPeriods * 6 / 4);
		}
		
		List<Integer> longRange = new ArrayList<>();
		if (longPeriods < 4) {
			if (longPeriods > 1) {
				longRange.add(longPeriods - 1);
			}
			longRange.add(longPeriods);
			longRange.add(longPeriods + 1);
		} else if (longPeriods < 8) {
			longRange.add(longPeriods / 2);
			longRange.add(longPeriods);
			longRange.add(longPeriods * 3 / 2);
		} else {
			longRange.add(longPeriods * 2 / 4);
			longRange.add(longPeriods * 3 / 4);
			longRange.add(longPeriods);
			longRange.add(longPeriods * 5 / 4);
			longRange.add(longPeriods * 6 / 4);
		}
		Iterator<Integer> longIterator = longRange.iterator();
		while (longIterator.hasNext()) {
			int possibleLong = longIterator.next();
			if (possibleLong > initialPeriodsAvailable) {
				LOGGER.info("Not enough pre-pricing to support a long average of " +
						possibleLong + " periods.");
				longIterator.remove();
			}
		}
		
		LOGGER.info("Bracketing short average over " + 
				shortRange.stream().map(n -> "" + n)
					.collect(Collectors.joining(", ", "[", "]")) + 
				" periods.");
		LOGGER.info("Bracketing long average over " + 
				longRange.stream().map(n -> "" + n)
					.collect(Collectors.joining(", ", "[", "]")) + 
				" periods.");
		LOGGER.info(String.format("Bracketing exit threshold from %1.4f to %1.4f " +
				"with an increment of %1.4f.", threshold / 2, threshold * 2, threshold / 4));
		
		List<TwoMovingAveragesTrader> result = new ArrayList<>();
		int ID = 0;
		for (int shortCandidate : shortRange) {
			for (int longCandidate : longRange) {
				if (shortCandidate < longCandidate) {
					for (int numerator = 2; numerator <= 8; ++numerator) {
						TwoMovingAverages candidate = new TwoMovingAverages
							(target.getStock(), target.getSize(), 
								shortCandidate * Pricing.SECONDS_PER_PERIOD * 1000, 
								longCandidate * Pricing.SECONDS_PER_PERIOD * 1000, 
								threshold * numerator / 4);
						candidate.setId(++ID);
						TwoMovingAveragesTrader trader = new TwoMovingAveragesTrader
								(pricing, market, persistence);
						trader.setStrategy(candidate);
						result.add(trader);
					}
				}
			}
		}
		
		return result;
	}
}
