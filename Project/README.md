# Algorithmic Trading Application

For your final application-development project, you will work in teams of three to complete and then to improve an application that supports high-frequency stock traders in managing sets of automated/algorithmic trading agents.

There is a list of mandatory tasks, including fixes to known issues with the application and creation of a CI/CD infrastructure that lets us deploy the application to an OpenShift server; and then you can either choose from among suggested application enhancements or propose your own. You will also work with a delegate from production support to complete deployment of the application to UAT and production servers.

You will demonstrate your completed and deployed application at the Tech Fair on the final day of the training program, and will give a presentation showing your command of the application's architecture and supporting technology and methodology.

Instructors will provide support and will also act as your client or customer. You will meet at least once with an instructor for advice on development strategies and technique, and once in order to review feature choices and UI design.

## Background

In a traditional market-analyst workstation, the user monitors stock prices and other data, and manually triggers trades: some number of shares of some stock, taking either a "long" position by buying the stock in hopes of selling it at a higher price or "shorting" the stock by selling it and planning to buy it back at a lower price. In our algorithmic trading workstation, the user instead creates automated traders that have certain marching orders to buy and sell on their own, and then monitors their activity, often stopping unprofitable traders, creating new ones, and tuning operating parameters.

Once activated, an algorithmic trader will trade more frequently than a human trader who makes each buy/sell decision and triggers each trade manually. For purposes of developing, testing, and especially demonstrating our application, we speed things up even further: stock pricing is available at 15-second intervals, and a trader might make a trade as frequently as once every minute or so.

There are many well-known trading algorithms, and others can easily be invented. Reliably profitable algorithms, of course, are fewer and farther between. The starter application implements one algorithm known as Two Moving Averages, or 2MA, and you can read about other common ones in the Resources section, below.

## Model and Terms

There can be some confusion between an algorithm, the specific application of that algorithm with specific parameters, and the active software entity that carries out trading according to that algorithm with those parameters. We use these terms in the application code and the remaining discussion:

* An **algorithm** is some specific logic for trading, based on a specific theory. For example, 2MA posits that, if we can detect that a stock's price is departing from the norm to some significant degree, it is likely to continue diverging in that direction, at least long enough for us to make some money by playing it that way, and the logic is that we take the crossing of the line of a shorter-term rolling average over the line of a longer-term one as the signal of that departure -- and that we then wait for a fixed-percentage profit or loss before closing the resulting position.

* A **strategy** is the application of an algorithm to a specific stock, for a specific number of shares, with specific parameters: for example, let's try trading up to 1000 shares of Honeywell at a time, opening a position when a 2-hour average crosses over a 4-hour average, and taking profit or loss at 3.00%.

* A **trader** is the active piece of software tasked with executing a strategy. From the user's perspective, strategy and trader are the same thing; but for the developer the strategy is just the state that drives the trader, and the trader is active, running code that might make trades. For another perspective on this, consider that a strategy has a persistent representation, while only in the running application is that strategy incarnated into a running trader.

The strategy's state includes a history of trades made (by the associated trader). These in turn are organized into **positions**: the trader always trades with a purpose, and all of our algorithms follow the pattern that they look for some trading signal in order to open a position, and then they look to close that position before going back on the hunt. A trader applies one set of criteria to incoming price data, and opens a long position by buying, and a short position by selling; then, it applies other criteria to decide when to close the position, by making the opposite trade to its opening trade -- that is, selling out of a long position, or buying out of a short one.

A position is "open" if it doesn't yet have a closing trade, and once "closed" it's over. Only a closed position has profit or loss, and a strategy's profit or loss is the sum of the profit/loss of all of its closed positions to date.

## Architecture

The application manages a relational database in which strategies, positions, and trades are stored and updated. Pricing history for stocks is also stored as it is watched by running traders, to support auditing and analytics after the fact.

The application relies on two external services, which are considered to be out of our control: a **PriceService** that provides a stream of stock pricing data on demand, and an **OrderBroker** that acts as our intermediary to the stock market itself (though it is actually not an intermediary but rather a simulation of market responses to our trade requests). The price service is a synchronous, HTTP request/response service; and the order broker is available over an asynchronous-messaging broker.

## Resources

You can get all of the source code and a few other resources by cloning/pulling the repository:

    http://bitbucket.org/wprovost/CitiMiss2019

Everything you need is found under **Project** in that repository:

* The starter back-end application, as an Eclipse project, is in **HFTrader**.

* The starter front-end application, as an Angular project, is in **HFTraderWeb**.

* The sources for the backing services required by the application are in **PriceService** and **OrderBroker**. Note that you do not need to run these projects, as the services are running on a public host. They are provided for your reference, and as a possible fallback in case the public services are unavailable.

* A description of algorithmic trading strategies is found in **TradingStrategies.pdf**.

* An Excel workbook with sample pricing data and notes as to how an algorithm might trade on the sample data is in **TestPricing.xls**. This data was used in the development of some of the unit tests in the starter application.


## Getting Started

To run the starter application, do the following:

1. Start the Apache Derby database server: run **startNetworkServer.bat** from the **bin** directory of your Derby installation.

1. Use Derby's **ij** to ...
    connect 'jdbc:derby://localhost/Trading;create=true';
    run '__your-local-repo__/HFTrader/data/create_db.sql';

1. Copy the **HFTrader** project in as a subfolder of your Eclipse STS workspace, and import it.

1. You'll see an error in **src/main/java/com/citi/trading/Market.java**. Fix up the property value in **src/main/resources/dev.properties**, as described in that offending line in the Java code, and then clean up the offending line.

1. Run **src/main/java/com/citi/trading/HFTrader** as a Java application.

1. In a fresh copy of NodeJS, install the Angular CLI.

1. Create a new Angular application called **HFTraderWeb**.

1. Remove the **src** folder from the new project.

1. Copy the **HFTraderWeb** content into your new application folder -- letting it overwrite existing config and source files.

1. From the application directory, and with your NodeJS in the executable path, run **npm install** to install necessary Node modules.

1. Run **ng serve** to build and host the application.

1. Visit the application in a browser:

    http://localhost:4200

Use the toolbar along the top to create a trader or two. For example, try a 2MA strategy on up to 1000 shares of MRK with short and long periods of 30 and 60 seconds, respectively, and an exit threshold of 3.00%. See the trader in the main table and, over a few minutes, it will start to make trades and show results. You can also see logging of activity in the back-end application.


## Frequently Asked Questions

1. "How does the user log in?" There is no login process, and there is only one user. In order to focus on the trading algorithms and other, more interesting matters, we are making the (perhaps dramatic) simplifying assumption that there is only one user. There is no login.

1. "Where is the user's portfolio?" a/k/a "How do we know how many shares we have to buy or sell?" For purposes of this application, you can assume unlimited cash and a portfolio sufficient to support any and all strategies put in play by the user. Remember that each trader ventures only N shares, maximum, for all time: it will always sell out or buy back before opening a new position.

1. "What stocks will the user trade?" You may choose in your UI explicitly to support symbols from specific markets, or not. The application will function fine on any stock symbol -- even a hypothetical one. The subtler question is, what behavior can we expect from a given symbol? Take a look at the source code for the **PriceService**, and see that a handful of symbols are designed to move according to different "profiles." This is all simulated data, of course, but some stocks will be completely predictable -- good for testing purposes, and for reliable trading activity come demonstration time -- and some move in more and less natural, randomized ways. Some profiles are quite volatile and some move on longer waves.


## Requirements

The following actions are required for your completed application:

1\. The application requires a CI/CD infrastructure, and must be deployed via Jenkins pipeline builds to OpenShift development, UAT, and production servers.

* You'll begin with the development server of course, and then work with a liaison from production support to promote it through testing and on to production.

* Regular builds with a simple Jenkins instance connected to your front- and back-end repositories should be conducted throughout your development process, as well.

* You must develop meaningful UA tests and work with your PS liaison to see them running successfully in UAT.

* There must be a way to vary settings per deployment: for example, you will set secure passwords on the database, and these must vary: at a minimum the credentials for production must be different from those for dev and UAT. The reply queue name sent to the order broker must vary as well, and backing URLs for ActiveMQ and the price service should be easily configurable.

2\. The application has a major flaw right now, which in short is that it doesn't take the result of a trade request into account. There are four possible outcomes from a trade request: filled, partially filled, rejected ... and it's possible, though uncommon, to get no response from the order broker at all. The **Market** component handles that last possibility, giving up on confirmation after a 15-second timeout, and assuming that the trade was rejected. So our algorithmic traders must deal with those first three possibilities: filled, partial, rejected.

Currently, the base **Trader** class always opens a position -- even if its requested opening trade was rejected! And it automatically closes, again regardless of the result of its closing-trade request. This can lean to logging trades that never happened, and for example buying 1000 shares to open a long position and then "closing" it after selling only 400 of those shares.

So the trader must be refined to recognize and react to non-fill results:

* When opening a position, a partial fill is fine, and we just record that we opened a position on __M__ shares instead of the __N__ we requested. But a rejected trade means, oh well, we missed our chance -- do not open the position, just stay on the hunt for the next trading signal.

* Only a complete fill closes a position. If a closing-trade request is rejected, we just keep waiting to close. A partial when closing is the trickiest thing: you need to __split__ the position into two records, as if you with perfect foresight had bought two chunks of stock at the same time: __M__ shares and then separately __N-M__ shares. Now, you can cleanly close the first position, and the new, second position remains open. Note that you could get a sequence of partial fills, so an initial position on 1000 shares could turn into positions of 400, 240, and a final 360 shares. Only when the last position is closed does the trader go back on the hunt for new trading signals.

Support your code enhancements with unit testing: in fact, best to start with new test cases that fail, right? and then improve the code to pass the new tests.

3\. You'll see that the user can create a trader, and then stop and start trading with those parameters over time. But, what happens if the user wants to stop a trader that has an open position? Or is in the midst of a trade, having requested it and now waiting for a response from the order broker? Those replies are usually pretty quick but can take up to 10 seconds.

The current application doesn't consider these possibilities at all, and simply stops a trader when the user requests it. This can lead to stopped traders with open positions, which is awkward; it can also lead to trades requested, actually made on the market, and not recorded, which is unacceptable: the user could own stock, long-term, and not even know it! So you'll need to clean this up, with some state management in the back-end application, and the UI will need some adjustments to stay in synch.

4\. The starter UI shows a summary table of traders and their activity, started/stopped state, and profitability. Add a feature such that the user can click through any one of those traders to see details on its trading history. Design the UI for this as you like. It must show all trades for that trader, with individual profitability, and it must update automatically as an active trader continues to trade.

5\. The application supports only one algorithm, which is Two Moving Averages. Implement at least one more. Bollinger Bands is the simplest next alternative, as documented in the trading-algorithms description shown in the Resources section.

Note that this improvement will have impacts in multiple layers of the application, from the database and persistence classes up through the business logic and out in the front end as well. Look closely at the infrastructure that's already in place to support multiple algorithms: base- and derived-type tables in the database schema, base and derived Java classes at more than one level ... and, oops, not really much support for this in the UI yet at all. Work with the structure that's there, and create your own as needed in the front end.

6\. This UI needs more than a coat of paint! It has some styling, but it wouldn't be mistaken for a professional web user experience, would it? Make this better: improve the styling, add graphics, charts, whatever you think will improve the user experience. (In the process, look to differentiate your application from those of other teams; this is more purely for purposes of demonstration, as you'll be presenting side-by-side at the Tech Fair.)


## Application Enhancements

Also implement some enhancements of your own. What do you think the application could use / where would you take it next? Imagine new capabilities for the application, do some cursory design, and consult with an instructor before moving into implementation.

And/or, here are some ideas, roughly from smallest to most ambitious:

1\. A third or even a fourth algorithm option.

2\. Filtering, sorting, and grouping of the traders table -- by stock, by profitability, started/stopped state, etc.

3\. Stronger input validation, in he UI as well as the REST service.

4\. Create a "trial-run" option, so the user could create a trader and watch it trade __hypothetically__, as a way of exploring what works and what doesn't without risking actual money.

5\. Wouldn't it be cool if the application offered the user some analysis and the possibility to tune a trader to be more profitable over time? Note that the application already stores pricing history as traders trade. So you could write a utility that would, for a specific, target trader, go back over the trading history and try out alternatives --
"fuzzing" the trader's parameters by a little or a lot, and seeing what changes would have resulting in more or less success.

For example, let's say a 2MA trader trades on AAPL with a short average of 60 seconds, a long average of 120 seconds, and an exit threshold of 3.00%, and runs for the whole morning. At lunch, the user might ask the application to analyze performance against pricing history so far. The application might run a bunch of nearby alternatives, and discover that -- with the benefit of hindsight at least -- the trader would have done better with a short average of 45 seconds, or an exit threshold of 2.25%, or both. Is this predictive for the afternoon's trading? That's a hard thing to say, but at least the user now has the idea, and might choose to act on it.

This is a bigger thing to tackle: higher effort and bigger reward.

6\. As to that stored pricing: that will grow in our database, without bound. Add a scheduled service of some sort that will clean out price points older than some expiration time.


## Schedule

### Thursday, July 18

We will spend the morning in study. Review the starter code and get good and familiar with the architecture and inner workings of the application. Learn the data model. Look at the Java application code: what components depend on what other components, and how are they wired together? How is the Angular application organized?

* Produce at least one coherent diagram that shows the components and relationships -- and several diagrams would be better.

* Be prepared to answer questions posed by the instructor after lunch. The goal here is that you understand the starter code base very well, before you jump into maintaining and improving it.

In the afternoon, you will organize into project teams. Get situated and focus on the following tasks:

* Discuss with your team and start planning your work: what goals and milestones will you plan for what days and times? Who will do what? Consider the specific deadlines shown later in this schedule section.

* Map out a strategy for Dockerizing and "OpenShift-ing" this application. Identify challenges in the application architecture, and plan to address these and to meet the deployment requirements specified above.

* Meet with an instructor to discuss your development goals and plan and your proposed individual roles. The instructors will set a schedule for this, and you may meet this afternoon or tomorrow morning.

* Get your team's shared repositories together. Remember **.gitignore** -- you get one for free in the Angular project, but you'll need one for the Eclipse project, and you want this in place __before__ pushing the starter code base up to BitBucket. One tip: you'll start to see a **derby.log** file in the Eclipse project folder, and you don't want to include that file. Look for other files or folders that might give you trouble.

* Get a Jenkins CI server attached to your repository or repositories. Your build must run unit tests for both front and back ends. Set a schedule to run your build periodically -- once an hour? -- or, set commit triggers so that each new push or approved pull request.

### Friday, July 19

Begin or continue your work on required tasks.

Each team will meet with an instructor a second time, the instructor for this second meeting playing the role of your client/customer. Plan to have clear proposals for things that call for client review and feedback.

By the end of the day, your team should have the starter application, possibly with your earliest fixes and enhancements, running on the OpenShift development server, and you'll be able to re-run your build pipeline at will.

### Monday, July 22

Today you will begin to work with your PS liaison. The ultimate goal is successful deployment and support of your application on development, UAT, and production servers. Start by familiarizing the PS resource with your application's features and especially its deployment process and status. Discuss challenges in moving it from development on to UAT and from there to production, and plan your interactions and set milestones for the week.

Continue development work, with regular CI builds and full OpenShift deployment as appropriate. By the end of the day, your application should be "fixed," with all required fixes and enhancements complete, and stable enough to promote to UAT.

### Tuesday, July 23

Continue development work, making the transition to optional features. Discuss your plans for these with the instructor, if you have not done so already.

Continue to work with PS to move your application through the release process. UAT should be complete by end of day.

### Wednesday, July 24

Continue development work, fleshing out and finalizing all features. Steer towards tomorrow's code freeze: the code should be highly stable by the end of the day. You may need to make some tough decisions about what original development goals are still realistic, which need to be scaled back, which perhaps need to be jettisoned; and how to move toward a stable code base.

Your application should be running, even if not in its final form, on the production server, and PS should be in a position to support it.

### Thursday, July 25

This morning is all about testing, stability, and robustness of your application. Whatever little quirks you've been living with in early development, it's time to stamp them out. You need to restart the database server every time you start the application? Time to clean that up. A test still fails on your CI build? Well, it would have been nice to address that back when it started failing, but you really must address it now. One of your algorithms is balky and occasionally gives insensible results? Fix it or, take a deep breath and remove or disable it. You present tomorrow, and it will feel a whole lot better to do so knowing that what you do have up and running is robust.

**Code freezes at 12:00,** and you will submit your team's repository for review and assessment, along with a brief description of the individual roles and contributions of each team member.

In the afternoon you will prepare technical presentations for tomorrow's Tech Fair. Topics will be assigned and expectations for the presentation explained and discussed. You will present once, to an assembled audience, on your team's assigned topic. You will also need a short presentation to be given ad-hoc at your demonstration tables, explaining what you were asked to do, how you did it, lessons learned, etc.

Think through what you want to say, prepare presentation materials, and **rehearse** your presentations, more than once. Instructors may be available to preview your presentations and give feedback. This preview will in no way be assessed, so, really, nothing to lose -- take advantage of it if you can.

### Friday, July 26 -- Tech Fair

A specific schedule for this day will be announced. There will be two main sessions, both to be held in the Annex:

1. Each team in sequence will give its technical presentation, to an audience of managers and other Citi staff, and time permitting will take a question or two. These presentations will be assessed. This occupies most of the morning.

1. After lunch, we move to an open-house mode in which each team will offer on-the-spot demonstrations of its running application and presentations of their work process.

We'll do a last group review, take some photos, and wrap up.


## Criteria for Submitted Work

Your application will be assessed with emphasis on the following criteria:

* Completed requirements and optional features: working cleanly and supported by unit tests and appropriate integration tests.

* CI/CD infrastructure: ongoing CI with Jenkins, appropriate Docker architecture, deployment to OpenShift using Jenkins pipelines, and "helping PS to help you" by supporting configuration in different target environments.

* Code quality: clear, readable code; self-descriptive identifiers for variables, fields, methods, classes, etc.; appropriate code documentation; any dead code and commented-out sections removed. It's not necessary to run style checkers but your code should have a reasonable shot at doing well if scanned with CheckStyle, TSLint, etc.

* Teamwork and planning: How well did you plan initially, and how well did you adapt? Did you do twice-daily standups -- and adjust your plans and responsibilities to stay productive? Did you work well together, review code, give advice? Did you communicate in order to manage your shared code base effectively, and avoid tie-ups and costly code merges? Were you responsive to queries and requirements from production support?

* Presentation: everyone involved in presentation, clear evidence of thought and planning, clear speech, eye contact, responsiveness to questions and challenges.

Submit information about your completed project using this [Google form](https://docs.google.com/forms/d/e/1FAIpQLSf7l_DyhyOzZNIm7E1YqwglNLQECtXQaRF58IECugWuSQi1ig/viewform).
