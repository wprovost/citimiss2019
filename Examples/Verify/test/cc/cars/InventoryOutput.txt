Item                        Quantity           Price           Value
------------------------    --------    ------------   -------------
2015 Toyota Prius                  1       28,998.99       28,998.99
2015 Subaru Outback                1       25,998.99       25,998.99
2014 Ford Taurus                   1       32,499.99       32,499.99
2015 Saab 9000                     1       34,498.99       34,498.99
2014 Honda Accord                  1       29,999.99       29,999.99
2014 Volkswagen Jetta TDI          1       23,899.99       23,899.99
2015 Kia Sonata                    0       21,999.99            0.00
2015 Ford F-150                    1       28,999.99       28,999.99
2014 Ford Escape Hybrid            1       23,999.99       23,999.99
2015 Audi A3                       1       39,999.99       39,999.99
1977 AMC Pacer                     1        1,998.99        1,998.99
1974 Ford Pinto                    1            0.99            0.99
1978 Renault Le Car                1        1,499.99        1,499.99
1991 Geo Metro                     1        1,098.99        1,098.99
1972 Ford El Camino                1        2,098.99        2,098.99
2004 Toyota Prius                  1        8,998.99        8,998.99
2004 Subaru Outback                1        6,998.99        6,998.99
2003 Ford Taurus                   1        9,499.99        9,499.99
2004 Saab 9000                     1       11,498.99       11,498.99
2003 Saturn Ion                    1        4,598.99        4,598.99
Brake hose                         8           30.49          243.92
Rearview mirror                    1           54.99           54.99
Distributor cap                    3           14.99           44.97
Headlight                          4           49.99          199.96
Fuse                              16            2.99           47.84
Spark plug                        16            4.99           79.84
