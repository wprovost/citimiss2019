/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars.sales;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import cc.cars.Car;
import cc.cars.SaleHandler;
import cc.cars.Salesman;

/**
Test for the {@link Simple Simple salesman}.

@author Will Provost
*/
public class SimpleTest
{
    private Car carOfMyDreams;
    private Salesman salesman;
    
    /**
    Create a test car -- the only value here that really matters is price --
    and build a simple salesman to sell that car.
    */
    @Before
    public void setUp ()
        throws Exception
    {
        carOfMyDreams =
            new Car ("", "", 2000, "", "", 10000, 0, 0, Car.Handling.GOOD);
        salesman = new Simple (carOfMyDreams);
    }
    
    /**
    Check the asking price.
    */
    @Test
    public void testAskingPrice ()
        throws Exception
    {
        assertEquals (9000, salesman.getAskingPrice (), .001);
    }
    
    /**
    Check that we can buy the car by offering above a 15%-discount threshold.
    */
    @Test
    public void testSaleSuccess ()
        throws Exception
    {
        SaleHandler handler = Mockito.mock (SaleHandler.class);
        salesman.onSale (handler);
        
        assertEquals (8900, salesman.willSellAt (8900), .001);
        Mockito.verify (handler).carSold (carOfMyDreams);
    }
    
    /**
    Check that we can't buy the car by offering below a 15%-discount threshold.
    */
    @Test
    public void testSaleFailure ()
        throws Exception
    {
        SaleHandler handler = Mockito.mock (SaleHandler.class);
        salesman.onSale (handler);
        
        assertEquals (9000, salesman.willSellAt (7500), .001);
        Mockito.verify (handler, Mockito.never ()).carSold (carOfMyDreams);
    }
}
