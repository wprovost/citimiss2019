/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import java.io.File;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
Test for the {@link Buyer}.

@author Will Provost
*/
public class BuyerTest
{
    /**
    Must clear out the persistent storage files before each test,
    so that we're getting a clean slate of not-yet-sold cars.
    */
    @Before
    public void setUp ()
        throws Exception
    {
        new File ("Dealership.cars").delete ();
        new File ("Dealership.usedCars").delete ();
        new File ("Dealership.parts").delete ();
    }
    
    /**
    Test a price negotiation with the 
    {@link cc.cars.sales.Standard Standard salesman}, setting a 
    system property to force the use of this salesman in the 
    {@link cc.cars.Dealership}, and using a generic negotiation policy.
    */
    @Test
    public void testBuyStandardSuccess ()
        throws Exception
    {
        System.setProperty 
            ("cc.cars.Salesman.type", "cc.cars.sales.Standard");
        Buyer buyer = new Buyer ("Toyota", "Prius", 26400, 4);
        assertEquals (26008.82, 
            buyer.buy (new Dealership (), 
                (a,b) -> Math.min(a, b) - 
                    Math.min (Math.abs(a - b), Math.min(a, b) / 2), 
                 (o, rsp, c, rnd, rnds) -> (c - o) * .7), 
             0.01);
    }

    /**
    Test a price negotiation with the 
    {@link cc.cars.sales.Standard Standard salesman}, setting a 
    system property to force the use of this salesman in the 
    {@link cc.cars.Dealership}, and using a negotiation policy
    optimized for this salesman type.
    */
    @Test
    public void testBuyStandardOptimized ()
        throws Exception
    {
        System.setProperty 
            ("cc.cars.Salesman.type", "cc.cars.sales.Standard");
        Buyer buyer = new Buyer ("Toyota", "Prius", 26400, 4);
        assertEquals (23384.79, 
            buyer.buy (new Dealership (), 
                (a,b) -> a * .7, 
                (o, rsp, c, rnd, rnds) ->  rnd == 3 ? rsp - o : 0), 
             0.01);
    }

    /**
    Test a price negotiation with the 
    {@link cc.cars.sales.HardNosed HardNosed salesman}, setting a 
    system property to force the use of this salesman in the 
    {@link cc.cars.Dealership}, and using a generic negotiation policy.
    */
    @Test
    public void testBuyHardNosedSuccess ()
        throws Exception
    {
        System.setProperty 
            ("cc.cars.Salesman.type", "cc.cars.sales.HardNosed");
        Buyer buyer = new Buyer ("Toyota", "Prius", 26400, 4);
        assertEquals (26032.89, 
            buyer.buy (new Dealership (), 
                (a,b) -> Math.min(a, b) - 
                    Math.min (Math.abs(a - b), Math.min(a, b) / 2), 
                 (o, rsp, c, rnd, rnds) -> (c - o) * .7), 
             0.01);
    }

    /**
    Test a price negotiation with the 
    {@link cc.cars.sales.HardNosed HardNosed salesman}, setting a 
    system property to force the use of this salesman in the 
    {@link cc.cars.Dealership}, and using a negotiation policy
    optimized for this salesman type.
    */
    @Test
    public void testBuyHardNosedOptimized ()
        throws Exception
    {
        System.setProperty 
            ("cc.cars.Salesman.type", "cc.cars.sales.HardNosed");
        Buyer buyer = new Buyer ("Toyota", "Prius", 26400, 20);
        assertEquals (24840.05, 
            buyer.buy (new Dealership (), 
                (a,b) -> a * .95, (o, rsp, c, rnd, rnds) ->  0), 
             0.01);
    }
}
