package com.citi.recipe.springdata;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Domain class for SQLRunner recipe, representing a person who let's say
 * bowls regularly in a league.
 *
 * @author Will Provost
 *
 */
@Entity
public class Bowler {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int ID;

    private String name;
    private int highScore;

    @Temporal(TemporalType.DATE)
    private Date gameDate;

    /**
     * Accessor for the ID property.
     */
    public int getID() {
        return ID;
    }

    /**
     * Mutator for the ID property.
     */
    public void setID(int iD) {
        ID = iD;
    }

    /**
     * Accessor for the name property.
     */
    public String getName() {
        return name;
    }

    /**
     * Mutator for the name property.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Accessor for the high-score property.
     */
    public int getHighScore() {
        return highScore;
    }

    /**
     * Mutator for the high-score property.
     */
    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    /**
     * Accessor for the game-date property.
     */
    public Date getGameDate() {
        return gameDate;
    }

    /**
     * Mutator for the game-date property.
     */
    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }
}
