package com.citi.recipe.springdata;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

/**
 * Spring Boot application that relies entirely on auto-configuration
 * to set up a database connection, JPA entity manager, and
 * Spring Data repository. We then exercise the repository.
 *
 * @author Will Provost
 */
@EnableAutoConfiguration
@PropertySource("classpath:dev.properties")
public class Application {

	/**
	 * Run the application as configured. Get the repository bean,
	 * and make a few calls to query for objects, add new ones, etc.
	 */
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);

		BowlerRepository repo = context.getBean(BowlerRepository.class);
        Bowler bowler = repo.findById(2).get();

        int highScore = bowler.getHighScore();
        System.out.println("Old high score was: " + highScore);
        bowler.setHighScore(highScore + 10);
        bowler.setGameDate(new Date());
        repo.save(bowler);

        Bowler bowler2 = repo.findById(2).get();
        System.out.println("New high score is:  " + bowler2.getHighScore());

        Bowler newBowler = new Bowler();
        newBowler.setID(4);
        newBowler.setName("Ignatius Isley");
        newBowler.setHighScore(100);
        newBowler.setGameDate(new Date());
        repo.save(newBowler);
        System.out.println("Added new bowler.");
	}
}