package com.citi.recipe.springdata;

import org.springframework.boot.test.context.SpringBootTest;

import com.citi.recipe.springdata.Application;

/**
 * This is not an integration test <em>per se</em>, because really
 * Spring Data doesn't give us much to test! It's more an illustration of
 * how the declared {@link BowlerRepository} functions.
 * This test requires that the database server be running and that
 * it support a test schema -- see create_db.sql.
 * This test is repeatable when it succeeds; it uses compensating operations
 * to put the DB back as it was found, but this can fail along with a test case.
 *
 * @author Will Provost
 */
@SpringBootTest(classes=Application.class)
public class BowlerRepositoryonnectedTest extends BowlerRepositoryTestBase {

}
