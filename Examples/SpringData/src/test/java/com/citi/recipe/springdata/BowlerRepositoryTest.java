package com.citi.recipe.springdata;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Isolated test. This is not a unit test <em>per se</em>, because really
 * Spring Data doesn't give us much to test! It's more an illustration of
 * how the declared {@link BowlerRepository} functions.
 *
 * @author Will Provost
 */
@SpringBootTest(classes=BowlerRepositoryTest.Config.class)
public class BowlerRepositoryTest extends BowlerRepositoryTestBase {

	/**
	 * For our test configuration, we just pull in a different properties
	 * file, which steers Spring Data to our in-memory database in lieu
	 * of the "real" network database used in the application and
	 * integration test.
	 */
	@Configuration
	@EnableAutoConfiguration
	@PropertySource("classpath:test.properties")
	public static class Config {
	}
}
