/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
Test suite for this package (and covers any sub-packages).
*/
@SuiteClasses
({ 
    CarTest.class, 
    DealershipTest.class, 
    PartTest.class, 
    cc.cars.sales.TestSuite.class
})
@RunWith(Suite.class)
public class TestSuite 
{
}
