/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import static org.junit.Assert.*;
import org.junit.Test;

/**
Test for the {@link Part} class.

@author Will Provost
*/
public class PartTest
{
    /**
    Initialize the part and test behavior as an inventory item.
    */
    @Test
    public void testPart ()
        throws Exception
    {
        Part target = new Part ("Muffler", 2, 99.99);
        assertEquals ("Muffler", target.getName ());
        assertEquals (2, target.getInStock ());
        assertEquals (2, target.getQuantity ());
        assertEquals (99.99, target.getPrice (), 0.0001);
    }
}
