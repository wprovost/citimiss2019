package cc.http;

import java.io.*;
import java.net.*;

/**
Utility to watch HTTP traffic.  This application will listen on one
local port and forward everything to another local port.  As it does so,
it gathers the HTTP header and body and writes them to the system
output stream and to a local file for later inspection.

@author Will Provost
*/
/*
Copyright 2002-2008 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/
public class HTTPSniffer
{
    /**
    Instantiates the class, passing the command-line arguments along to the
    constructor.
    */
    public static void main (String[] args)
    {
        System.out.println ();
        System.out.println ("                 HTTPSniffer                    ");
        System.out.println ("Copyright (c) 2002-2008 Capstone Courseware, LLC");
        System.out.println ("All rights reserved.  www.capstonecourseware.com");
        System.out.println ();
        
        if (args.length < 2)
        {
            System.out.println ("Usage: java cc.http.HTTPSniffer " +
                "<listen-port> <forward-port>");
            System.exit (-1);
        }
        
        new HTTPSniffer 
            (Integer.parseInt (args[0]), Integer.parseInt (args[1]));
    }
    
    /**
    Sets up log file and starts listening on the first port.
    Each time a message comes in, it forwards to the second port,
    gets the response and hands that back to the original requestor.
    Along the way, it grabs the HTTP content and dumps it to the log file
    and to the system output stream.
    */
    public HTTPSniffer (int listenPort, int forwardPort)
    {
        ServerSocket socket = null;
        try
        {
            echo = new PrintWriter (new FileWriter (dumpFile));
            socket = new ServerSocket (listenPort);
            System.out.println ("Forwarding local port " + listenPort +
                " to local port " + forwardPort + " ...");
            System.out.println ();
            
            while (true)
            {
                Socket listen = socket.accept ();
                Socket forward = new Socket ("LOCALHOST", forwardPort);

                passHTTPMessage (listen.getInputStream (), 
                                 forward.getOutputStream ());
                passHTTPMessage (forward.getInputStream (), 
                                 listen.getOutputStream ());
                
                forward.close ();
                listen.close ();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace ();
        }
        finally
        {
            try { socket.close (); } catch (Exception ex) {}
        }
    }
    
    /**
    Forwards an HTTP message and dumps formatted copies of the message
    to the log file and system output.
    */
    private synchronized void passHTTPMessage 
            (InputStream inStream, OutputStream outStream)
    {
        BufferedInputStream in = new BufferedInputStream (inStream, 256);
        BufferedOutputStream out = new BufferedOutputStream (outStream, 256);

        try
        {
            String line;
            int length = -1;
            boolean chunked = false;
            
            // Read header:
            while ((line = transferLine (in, out)) != null)
            {
                if (line.length () == 0)
                    break;

                final String CHUNKED = "Transfer-Encoding: chunked";
                final String LENGTH = "Content-length: ";
                
                // Detect chunked transmission:
                if (line.length () >= CHUNKED.length () && 
                    line.substring (0, CHUNKED.length ())
                        .equalsIgnoreCase (CHUNKED))
                    chunked = true;

                // Store content length:
                else if (line.length () >= LENGTH.length () && 
                    line.substring (0, LENGTH.length ())
                        .equalsIgnoreCase (LENGTH))
                    length = Integer.parseInt 
                        (line.substring (LENGTH.length (), line.length ()));
            }

            // Read body:
            boolean body = false;
            if (chunked)
            {
                chunks: while ((line = transferLine (in, out)) != null)
                {
                    body = true;

                    String chunkSize = line.toLowerCase ();
                    int size = 0;
                    for (int c = 0; c < chunkSize.length (); ++c)
                    {
                        size *= 16;
                        char ch = chunkSize.charAt (c);
                        size += (ch >= '0' && ch <= '9')
                            ? (int) (ch - '0')
                            : (int) (ch - 'a' + 10);
                    }
                    if (line.length () == 0)
                        continue chunks;

                    if (size != 0)
                        transferBytes (in, out, size);
                    else
                    {
                        out.write ('\r');
                        out.write ('\n');
                        dump ("");
                        break chunks;
                    }
                }
            }
            else if (length >= 0)
            {
                body = true;
                transferBytes (in, out, length);
            }

            if (!body)
                dump ("[No message body.]");

            dump ("");
        }
        catch (Exception ex)
        {
            ex.printStackTrace ();
        }
        finally
        {
            try { out.flush (); } catch (Exception ex) {}
            echo.flush ();
        }
    }

    private String transferLine (InputStream in, OutputStream out)
        throws IOException
    {
        StringBuilder result = new StringBuilder ();
        int c = 0;
        while ((c = in.read ()) != '\n')
        {
            out.write (c);
            if (c != '\r')
                result.append ((char) c);
        }
        out.write (c);
        
        dump (result.toString ());
        return result.toString ();
    }
    
    private void transferBytes (InputStream in, OutputStream out, int length)
        throws IOException
    {
        boolean binaryContent = false;
        for (int b = 0; b < length; ++b)
        {
            int r = in.read ();
            out.write (r);

            if (!binaryContent)
                if (r >127)
                {
                    binaryContent = true;
                    dump ("[Binary content transferred.]");
                }
                else
                {
                    echo.print ((char) r);
                    System.out.print ((char) r);
                }
        }
    }
    
    private void dump (String text)
    {
        echo.println (text);
        System.out.println (text);
    }
    
    private static final String dumpFile = "Traffic.txt";
    
    private PrintWriter echo;
}

