/*
Copyright 2016 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

import java.util.function.Predicate;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
General-purpose matcher that takes a description in advance,
along with an implementatin of the functional interface
<strong>Predicate&lt;T&gt;</strong>, which we then use as our matching test.
This adapts the Hamcrest Matcher design to Java-8 functional programming,
in that lambda expressions and method references can implement the predicate.

@author Will Provost
*/
public class FunctionalMatcher<T>
    extends TypeSafeMatcher<T>
{
    private Predicate<T> test;
    private String text;

    /**
    Provide matching logic as a <strong>Predicate&lt;T&gt;</strong>
    implementation, and a fixed string as the description.
    */
    public FunctionalMatcher (Predicate<T> test, String text)
    {
        this.test = test;
        this.text = text;
    }

    /**
    Test for a match by invoking the predicate for the given object.
    */
    public boolean matchesSafely (T object)
    {
        return test.test (object);
    }

    /**
    Append the provided description text.
    */
    public void describeTo (Description description)
    {
        description.appendText (text);
    }

    /**
    Following the fluent style of Hamcrest matchers, we provide this
    factory method as a self-descriptive way to invoke this matcher.
    */
    public static <T> FunctionalMatcher<T> exhibits
        (Predicate<T> test, String text)
    {
        return new FunctionalMatcher<T> (test, text);
    }
}
