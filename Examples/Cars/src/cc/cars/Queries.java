/*
Copyright 2014 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
Stream-based queries into the car inventory.

@author Will Provost
*/
public class Queries
{
    /**
    Helper method to report on a single car.
    */
    public static void show (Car car)
    {
        System.out.format ("%s: %s $%,1.2f%s%n", 
            car.getVIN (), car.getShortName (),
            car.getStickerPrice (),
            car instanceof UsedCar ? " -- USED" : "");
    }
    
    /**
    Helper method to report on an Optional&lt;Car&gt;, as provided by
    some stream operations.
    */
    public static void show (Optional<Car> car)
    {
        if (car.isPresent ())
            show (car.get ());
        else
            System.out.println ("No car found.");
    }
    
    /**
    Runs a series of stream operations on the new- and used-car collections,
    producing labeled results to the console.
    */
    public static void main (String[] args)
    {
        Persistence persistence = new Persistence ();
        List<Car> cars = persistence.loadCars ();
        List<UsedCar> usedCars = persistence.loadUsedCars ();

        System.out.println ("Find a single car by VIN:");
        show (Stream.concat (cars.stream (), usedCars.stream ())
            .filter (car -> car.getVIN ().equals ("ME3278"))
            .findFirst ());
        System.out.println ();

        System.out.println 
            ("Find cars of a certain model year:");
        cars.stream ().filter (car -> car.getYear () == 2015)
            .forEach (Queries::show);
        System.out.println ();

        System.out.println 
            ("Find new cars with price less than some threshold:");
        cars.stream ().filter (car -> car.getStickerPrice () < 30000)
            .forEach (Queries::show);
        System.out.println ();

        System.out.println 
            ("All cars of a certain make, sorted by price:");
        Stream.concat (cars.stream (), usedCars.stream ())
            .filter (car -> car.getMake ().equals ("Ford"))
            .sorted ((car1, car2) -> Double.compare 
                (car1.getStickerPrice (), car2.getStickerPrice ()))
            .forEach (Queries::show);
        System.out.println ();

        System.out.println ("New cars, grouped by model year:");
        for (Map.Entry<Integer, List<Car>> entry : cars.stream ()
            .collect (Collectors.groupingBy (car -> car.getYear ()))
            .entrySet ())
        {
            System.out.println ("Model year " + entry.getKey () + ":");
            for (Car car : entry.getValue ())
            {
                System.out.print ("  ");
                show (car);
            }
        }
        System.out.println ();

        System.out.println ("Average sticker price by model year:");
        for (Map.Entry<Integer, Double> entry : cars.stream ()
            .collect (Collectors.groupingBy (car -> car.getYear (),
                Collectors.averagingDouble ((Car car) -> car.getStickerPrice ())))
            .entrySet ())
        {
            System.out.format ("  Model year %4d: $%,11.2f%n", 
                entry.getKey (), entry.getValue ());
        }
        System.out.println ();
    }
}
