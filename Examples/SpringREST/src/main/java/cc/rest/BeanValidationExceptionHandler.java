/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
Global exception handler for Java-EE Bean Validation.

@author Will Provost
*/
@ControllerAdvice
public class BeanValidationExceptionHandler
{
    /**
    Converts validity failure to an HTTP 400, with a meaningful message.
    */
    @ExceptionHandler
    public ResponseEntity<String> onInvalidEntity
        (MethodArgumentNotValidException ex)
    {
        String message = String.format
            ("Validation of the request entity failed, " +
                "with the following messages:%n");
        for (FieldError error : ex.getBindingResult ().getFieldErrors ())
            message += String.format ("  %s: %s%n",
                error.getField (), error.getDefaultMessage ());

        return new ResponseEntity<String> (message, HttpStatus.BAD_REQUEST);
    }
}
