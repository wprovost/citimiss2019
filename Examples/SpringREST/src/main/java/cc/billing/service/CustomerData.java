/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing.service;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import cc.billing.Customer;
import cc.billing.CustomerService;
import cc.service.MapBackedService;

/**
CRUD service for customer records, using hard-coded data.

@author Will Provost
*/
@Component
public class CustomerData
    extends MapBackedService<Customer>
    implements CustomerService
{
    /**
    Provide the logic for getting and setting IDs to the
    superclass constructor.
    */
    public CustomerData ()
    {
        super (Customer.class, c -> c.getID(), (c,ID) -> c.setID(ID));
    }

    /**
    Add a series of customer records.
    */
    @PostConstruct
    public void init ()
    {
        Customer customer1 = new Customer ();
        customer1.setFirstName ("Ardent");
        customer1.setLastName ("Hornblower");
        customer1.setAddress1 ("123 Main Street");
        customer1.setCity ("Pittsfield");
        customer1.setState ("MA");
        customer1.setZIP ("01568");
        customer1.setEMail ("ardent@horns.com");
        add (customer1);

        Customer customer2 = new Customer ();
        customer2.setFirstName ("Frannie");
        customer2.setLastName ("Fickle");
        customer2.setAddress1 ("2 Penny Lane");
        customer2.setCity ("New London");
        customer2.setState ("CT");
        customer2.setZIP ("03021");
        customer2.setEMail ("franf@yahoo.com");
        add (customer2);


        Customer customer3 = new Customer ();
        customer3.setFirstName ("Third");
        customer3.setLastName ("Customer");
        customer3.setAddress1 ("3 Penny Lane");
        customer3.setCity ("New London");
        customer3.setState ("CT");
        customer3.setZIP ("03021");
        customer3.setEMail ("third@enders.com");
        add (customer3);

        Customer customer4 = new Customer ();
        customer4.setFirstName ("Fourth");
        customer4.setLastName ("Customer");
        customer4.setAddress1 ("4 Penny Lane");
        customer4.setCity ("New London");
        customer4.setState ("CT");
        customer4.setZIP ("03021");
        customer4.setEMail ("fourth@yahoo.com");
        add (customer4);

        Customer customer5 = new Customer ();
        customer5.setFirstName ("Fifth");
        customer5.setLastName ("Customer");
        customer5.setAddress1 ("5 Penny Lane");
        customer5.setCity ("New London");
        customer5.setState ("CT");
        customer5.setZIP ("03021");
        customer5.setEMail ("fifth@yahoo.com");
        add (customer5);
    }
}

