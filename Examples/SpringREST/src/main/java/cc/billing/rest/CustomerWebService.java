/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cc.billing.Customer;
import cc.rest.WebService;

/**
CRUD-style web service for customer records.

@author Will Provost
*/
@RestController
@RequestMapping("billing/customers")
@CrossOrigin(origins="http://localhost:4200")
public class CustomerWebService
    extends WebService<Customer>
{
}

