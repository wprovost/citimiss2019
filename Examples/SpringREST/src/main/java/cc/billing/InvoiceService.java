/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.billing;

import cc.service.CRUDService;

/**
CRUD service for Invoices.

@author Will Provost
*/
public interface InvoiceService
    extends CRUDService<Invoice>
{
    public static class NotFoundByNumberException
        extends NotFoundException
    {
        private int number;

        public NotFoundByNumberException (int number)
        {
            super (Invoice.class, 0);
            this.number = number;
        }

        public int getNumber ()
        {
            return number;
        }

        @Override
        public String getMessage ()
        {
            return "No invoice with number " + number;
        }
    }

    /**
    Gets the invoice with the given number, or null if not found.
    */
    public Invoice getByNumber (int invoiceNumber)
        throws NotFoundByNumberException;

    /**
    Creates the invoice and the associate customer records.
    */
    public void addInvoiceAndCustomer (Invoice invoice);
}

