/*
Copyright 2010-2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/
package cc.service;

import java.util.List;

/**
A generic service that offers CRUD operations for a certain class.

@author Will Provost
*/
public interface CRUDService<T>
{
    /**
    Dedicated exception for no-such-ID outcomes.
    */
    public static class NotFoundException
        extends Exception
    {
        private Class<?> type;
        private int ID;

        public NotFoundException (Class<?> type, int ID)
        {
            super ("No " + type.getSimpleName () +
                " found with ID " + ID + ".");

            this.type = type;
            this.ID = ID;
        }
        public Class<?> getType ()
        {
            return type;
        }

        public int getID ()
        {
            return ID;
        }
    }

    /**
    Dedicated exception for ID conflicts.
    */
    public static class ConflictException
        extends Exception
    {
        private Object candidate;
        private int ID;

        public ConflictException (Object candidate, int ID)
        {
            super ("There is already a " +
                candidate.getClass ().getSimpleName () +
                " with ID " + ID + ".");

            this.candidate = candidate;
            this.ID = ID;
        }

        public Object getCandidate ()
        {
            return candidate;
        }

        public int getID ()
        {
            return ID;
        }
    }

    /**
    Returns the total number of objects.
    */
    public long getCount ();

    /**
    Gets all instances of the managed class.
    */
    public List<T> getAll ();

    /**
    Returns the object with the given ID.
    */
    public T getByID (int ID)
        throws NotFoundException;

    /**
    Adds the given object to the data set, with a generated ID
    that is guaranteed to be unique.
    */
    public T add (T newObject);

    /**
    Adds the given object to the list, with given "natural" ID.
    */
    public void add (int ID, T newObject)
        throws ConflictException;

    /**
    Updates the object whose ID matches the given object with the
    given object's state.
    */
    public T update (T modifiedObject)
        throws NotFoundException;

    /**
    Removes the given object.
    */
    public void remove (T oldObject)
        throws NotFoundException;

    /**
    Removes the object with the given ID.
    */
    public void removeByID (int ID)
        throws NotFoundException;
}

