package cc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Host application for a couple of basic REST services.
 * 
 * @author Will Provost
 */
@SpringBootApplication
@PropertySource("classpath:application.properties")
public class SpringRESTApplication {

	/**
	 * Run the application as configured.
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringRESTApplication.class, args);
	}
}
