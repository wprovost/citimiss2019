package com.citi.recipe.springjms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.JmsListenerEndpointRegistry;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * JMS listener that receives and logs messages, and sends replies
 * to a separate queue.
 *
 * @author Will Provost
 */
@Component
public class TwoWayListener {

	@Autowired
	private ApplicationContext context;

	@Autowired
	private JmsTemplate jmsTemplate;

	/**
	 * Helper method to shut down the surrounding listener container
	 * and Java process.
	 */
	private void shutdown() {

		// Can't @Autowire the registry -- you'll get a
    	// permanently-empty one. Instead, inject the context,
    	// and look it up when needed:
    	JmsListenerEndpointRegistry registry =
    			context.getBean(JmsListenerEndpointRegistry.class);
		for (MessageListenerContainer container : registry.getListenerContainers()) {
			DefaultMessageListenerContainer def = (DefaultMessageListenerContainer) container;
			def.shutdown();
			System.out.println("Stopped listener container.");
		}

		// That gives us a clean disengagement from the JMS queue;
		// we still have to shut down the Java process:
		try { Thread.sleep(5000); } catch (InterruptedException ex) {}
		System.exit(0);
	}

	/**
	 * The listener method will be recognized by Spring-JMS auto-configuration.
	 * It prints the contents of the expected text message, and then uses
	 * an injected <strong>JmsTemplate</strong> to send a reply to a separate
	 * queue. If the message is "shutdown" (case-insensitive) it will shut
	 * down as a service.
	 */
	@JmsListener(destination=Constants.TWO_WAY_REQUEST_QUEUE)
    public void onMessage(String text) {
        System.out.println("Received message: " + text);

        jmsTemplate.convertAndSend(Constants.TWO_WAY_REPLY_QUEUE, "Reply to: " + text);

        if (text.equalsIgnoreCase("SHUTDOWN")) {
        	new Thread(this::shutdown).start();
        }
    }
}
