package com.citi.recipe.springjms;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.core.JmsTemplate;

/**
 * Spring Boot application that configures a component that can send text
 * messages, and the necessary Spring JMS resources to support that component.
 * Note that we don't use Spring-JMS auto-configuration: if we did,
 * we'd wind up with a JMS listener container and a process that would
 * run ad infinitum. We just want to start, configure, send, and shut down.
 * In fact this whole approach might be overkill, given our minimal
 * requirements, and other examples of sending messages with Spring JMS
 * just use the connection factory and <strong>JmsTemplate</strong>
 * programmatically.
 *
 * @author Will Provost
 */
@Configuration
@PropertySource("classpath:application.properties")
public class OneWaySenderApplication {

	/**
	 * This lets us inject the URL of the JMS message broker from the same
	 * properties file that's used by the corresponding listener application.
	 */
	@Value("${spring.activemq.broker-url}")
	public String brokerURL;

	/**
	 * Creates a connection factory with the injected broker URL.
	 */
	@Bean
	public ConnectionFactory connectionFactory() {
		return new ActiveMQConnectionFactory(brokerURL);
	}

	/**
	 * Creates a JMS template with our connection-factory bean.
	 */
	@Bean
	public JmsTemplate jmsTemplate() {
		return new JmsTemplate(connectionFactory());
	}

	/**
	 * Creates the sending component, which will be auto-wired to the
	 * required <strong>JmsTemplate</strong> bean.
	 */
	@Bean
	public OneWaySender oneWaySender() {
		return new OneWaySender();
	}

	/**
	 * Run the application as configured. Get the sending bean, and
	 * pass it some text to send.
	 */
	public static void main(String[] args) {
		try ( AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(OneWaySenderApplication.class); ) {
			OneWaySender sender = context.getBean(OneWaySender.class);
			sender.send(args.length == 0
	                ? "Testing SpringJMS recipe, one way" : args[0]);
		}
	}
}
