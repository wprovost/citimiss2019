package com.citi.recipe.mq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.recipe.springjms.OneWayListener;
import com.citi.recipe.springjms.TwoWayListener;

/**
 * Unit test for the {@link OneWayListener} class.
 * In contrast to the {@link OneWayListenerTest}, here we do use
 * Spring configuration around our test, mostly so that we can fix up
 * a mock <strong>JmsTemplate</strong> for the component to use in
 * sending its reply message.
 *
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TwoWayListenerTest.Config.class)
public class TwoWayListenerTest {

	/**
	 * Test configuration, with a Mockito-mocked <strong>JmsTemplate</strong>
	 * and the component under test.
	 */
	@Configuration
	public static class Config {

		@Bean
		public JmsTemplate jmsTemplate() {
			return Mockito.mock(JmsTemplate.class);
		}

		@Bean
		public TwoWayListener twoWayListener() {
			return new TwoWayListener();
		}
	}

	@Autowired
	public TwoWayListener listener;

	@Autowired
	private JmsTemplate mockTemplate;

	/**
     * "Send" the listener a message, and then verify that the component
     * did send the correct reply to the mock JMS template.
	 */
    @Test
    public void testOnMessage() throws Exception {

    	listener.onMessage("Test message");
        Mockito.verify(mockTemplate).convertAndSend
        		("TwoWayReplyQueue", "Reply to: Test message");
    }
}
