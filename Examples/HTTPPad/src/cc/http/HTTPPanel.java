/*
Copyright 2002-2014 by Capstone Courseware, LLC, and William W. Provost.
All rights reserved.
*/

package cc.http;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import cc.xml.PrettyPrinter;

/**
Panel to manage the request and response text.
It must be fed the host, port, and endpoint information.

@author Will Provost
*/
public class HTTPPanel
    extends JPanel
{
    /**
    Builds GUI and connects event handlers.
    */
    public HTTPPanel ()
    {
        httpClient = new HTTPClient ();

        setLayout (new BorderLayout (4, 4));

        JPanel request = new JPanel ();
        request.setLayout (new BorderLayout ());

        JPanel buttons = new JPanel ();
        buttons.setLayout (new FlowLayout (FlowLayout.CENTER));
        buttons.add (bnSend);

        request.add (BorderLayout.NORTH, new JLabel ("Request:"));
        request.add (BorderLayout.CENTER, new JScrollPane (txRequest));
        request.add (BorderLayout.SOUTH, buttons);

        txRequest.setFont (fixedFont);
        request.setBorder (BorderFactory.createEmptyBorder (4, 4, 4, 4));

        ButtonGroup viewOptions = new ButtonGroup ();
        JPanel responseOptions = new JPanel ();
        for (JRadioButton bn : formatButtons)
        {
            viewOptions.add (bn);
            responseOptions.add (bn);
        }

        JPanel responseHeader = new JPanel ();
        responseHeader.setLayout (new BorderLayout ());
        responseHeader.add (BorderLayout.WEST, new JLabel ("Response:"));
        responseHeader.add (BorderLayout.EAST, responseOptions);

        JPanel response = new JPanel ();
        response.setLayout (new BorderLayout ());
        response.add (BorderLayout.NORTH, responseHeader);
        response.add (BorderLayout.CENTER, new JScrollPane (txResponse));
        txResponse.setFont (fixedFont);
        response.setBorder (BorderFactory.createEmptyBorder (4, 4, 4, 4));

        Sender sender = new Sender ();
        Keymap map = JTextComponent.addKeymap ("HTTPPad",
            JTextComponent.getKeymap (JTextComponent.DEFAULT_KEYMAP));
        map.addActionForKeyStroke (KeyStroke.getKeyStroke (KeyEvent.VK_INSERT,
            Event.CTRL_MASK), new DefaultEditorKit.CopyAction ());
        map.addActionForKeyStroke (KeyStroke.getKeyStroke (KeyEvent.VK_INSERT,
            Event.SHIFT_MASK), new DefaultEditorKit.PasteAction ());
        map.addActionForKeyStroke (KeyStroke.getKeyStroke (KeyEvent.VK_DELETE,
            Event.SHIFT_MASK), new DefaultEditorKit.CutAction ());
        map.addActionForKeyStroke (KeyStroke.getKeyStroke (KeyEvent.VK_ENTER,
            Event.CTRL_MASK), sender);
        txRequest.setKeymap (map);
        txResponse.setKeymap (map);

        JSplitPane split = new JSplitPane
            (JSplitPane.VERTICAL_SPLIT, request, response);
        split.setResizeWeight (.5);
        split.setDividerLocation (250);
        add (BorderLayout.CENTER, split);

        txResponse.setEditable (false);

        ActionListener viewChanger = new ViewChanger ();
        for (JRadioButton bn : formatButtons)
            bn.addActionListener (viewChanger);

        bnSend.addActionListener (sender);
    }

    /**
    Accessor for the request property.
    */
    public String getRequest ()
    {
        return txRequest.getText ();
    }

    /**
    Mutator for the request property.
    */
    public void setRequest (String newValue)
    {
        txRequest.setText (newValue);
    }

    /**
    Accessor for the root URL -- this is a pass-through to the HTTPClient.
    */
    public String getRoot ()
    {
        return httpClient.getRoot ();
    }

    /**
    Mutator for the root URL -- this is a pass-through to the HTTPClient.
    */
    public void setRoot (String newValue)
    {
        httpClient.setRoot (newValue);
    }

    /**
    Accessor for the response property.
    */
    public String getResponse ()
    {
        return txResponse.getText ();
    }

    /**
    Mutator for the response property.
    */
    public void setResponse (String newValue)
    {
        response = newValue;
        formatResponse ();
    }

    /**
    Accessor for the format selection.
    */
    public String getFormat ()
    {
        for (JRadioButton bn : formatButtons)
            if (bn.isSelected ())
                return bn.getText ();

        return "None";
    }

    /**
    Mutator for the response property.
    */
    public void setFormat (String newValue)
    {
        for (JRadioButton bn : formatButtons)
            if (bn.getText ().equals (newValue))
                bn.setSelected (true);
    }

    /**
    Format the given message body as pretty-printed XML.
    */
    private String prettyPrintXML (String body)
    {
        try
        {
            return PrettyPrinter.format (body, null);
        }
        catch (Exception ex)
        {
            System.out.println (ex.getClass ().getName () +
                " exception while formatting XML:");
            System.out.println ("  " + ex.getMessage ());
        }

        return body;
    }

    /**
    Format the given message body as pretty-printed JSON.
    */
    private String prettyPrintJSON (String body)
    {
        try
        {
            Object parsed = new JSONTokener (body).nextValue ();
            if (parsed instanceof JSONObject)
                return ((JSONObject) parsed).toString (2);

            return parsed.toString ();
        }
        catch (JSONException ex)
        {
            System.out.println (ex.getClass ().getName () +
                " exception while formatting JSON:");
            System.out.println ("  " + ex.getMessage ());
            System.out.println ("For string: >>>" + body + "<<<");
        }

        return body;
    }

    /**
    Auto-format all response bodies based on response content type.
    */
    private String autoFormat (String response)
    {
        final String HTTP_11 = "HTTP/1.1 ";
        final String CONTENT_TYPE = "Content-Type:";
        final String END_LINE = "\r\n";

        try
        (
            BufferedReader in = new BufferedReader
                (new StringReader (response));
        )
        {
            StringBuilder builder = new StringBuilder ();
            String contentType = "NOT FOUND";
            String line;

            do
            {
                boolean foundResponse = false;
                while ((line = in.readLine ()) != null &&
                    (line.length () != 0 || !foundResponse))
                {
                    if (line.startsWith (HTTP_11))
                        foundResponse = true;

                    if (line.startsWith (CONTENT_TYPE))
                        contentType = line.substring
                            (CONTENT_TYPE.length ()).trim ();

                    builder.append (line).append (END_LINE);
                }

                if (line == null)
                {
                    builder.append (END_LINE);
                    break;
                }

                StringBuilder body = new StringBuilder ();
                boolean first = true;
                while ((line = in.readLine ()) != null &&
                    !line.startsWith ("--") && !line.startsWith ("=="))
                {
                    if (!first)
                        body.append (END_LINE);
                    body.append (line);
                    first = false;
                }

                builder.append (END_LINE);
                String trimmed = body.toString ().trim ();
                if (trimmed.length () != 0 &&
                        (contentType.equals ("text/xml") ||
                         contentType.equals ("application/xml")))
                    builder.append (prettyPrintXML (trimmed));
                else if (trimmed.length () != 0 &&
                        contentType.equals ("application/json"))
                    builder.append (prettyPrintJSON (trimmed));
                else
                    builder.append (body.toString ());

                builder.append (END_LINE);
                if (line != null)
                    builder.append (line).append (END_LINE);
            }
            while (line != null);

            return builder.toString ();
        }
        catch (IOException ex)
        {
            ex.printStackTrace ();
        }

        return response; // bail out of formatting and give back the raw text
    }

    /**
    Format according to radio-button selection.
    */
    private void formatResponse ()
    {
        txResponse.setContentType
            (bnHTML.isSelected () ? "text/html" : "text/plain");

        if (bnText.isSelected () || response.length () == 0)
            txResponse.setText (response);
        else if (bnAuto.isSelected ())
            txResponse.setText (autoFormat (response));
        else
        {
            int boundary = response.indexOf ("\n\n") + 2;
            String headers = response.substring (0, boundary);
            String body = response.substring (boundary);

            if (bnHTML.isSelected ())
                txResponse.setText (body);
            else if (bnXML.isSelected ())
                txResponse.setText (headers + prettyPrintXML (body));
            else if (bnJSON.isSelected ())
                txResponse.setText (headers + prettyPrintJSON (body));
        }

        txResponse.setCaretPosition (0);
    }

    /**
    Call this method to trigger the request.  After this call the
    {@link #getResponse getResponse} method can be used to retrieve
    the response.
    */
    public void doRequestResponse ()
    {
        String requestText = txRequest.getText ();
        String responseText = httpClient.invokeScript (requestText);
        setResponse (responseText);
    }

    /**
    This handler kicks off messaging based on user input by calling
    {@link #doRequestResponse doRequestResponse}.
    */
    private class Sender
        extends AbstractAction
    {
        public void actionPerformed (ActionEvent ev)
        {
            doRequestResponse ();
        }
    }

    private class ViewChanger
        implements ActionListener
    {
        public void actionPerformed (ActionEvent ev)
        {
            formatResponse ();
        }
    }

    private HTTPClient httpClient;
    private String response = "";

    private JTextArea txRequest = new JTextArea ();

    private JRadioButton bnText = new JRadioButton ("Text", true);
    private JRadioButton bnHTML = new JRadioButton ("HTML");
    private JRadioButton bnXML = new JRadioButton ("XML");
    private JRadioButton bnJSON = new JRadioButton ("JSON");
    private JRadioButton bnAuto = new JRadioButton ("Auto");
    private JRadioButton[] formatButtons =
        { bnText, bnHTML, bnXML, bnJSON, bnAuto };

    private JEditorPane txResponse = new JEditorPane ();
    private JButton bnSend = new JButton ("Send");

    private static Font fixedFont = new Font ("Courier",
        HTTPPad.FONT_WEIGHT.equalsIgnoreCase ("BOLD") ? Font.BOLD : Font.PLAIN,
        Integer.parseInt (HTTPPad.FONT_SIZE));
}
