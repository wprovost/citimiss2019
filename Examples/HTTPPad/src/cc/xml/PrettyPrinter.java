/*
Copyright 2002-2010 by Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
This class "pretty prints" an XML stream to something more human-readable.
It duplicates the character content with some modifications to whitespace,
restoring line breaks and a simple pattern of indenting child elements.

This version of the class acts as a SAX 2.0 <code>DefaultHandler</code>,
so to provide the unformatted XML just pass a new instance to a SAX parser.
Its output is via the {@link #toString toString} method.

One major limitation:  we gather character data for elements in a single
buffer, so mixed-content documents will lose a lot of data!  This works
best with data-centric documents where elements either have single values
or child elements, but not both.

@author Will Provost
*/
public class PrettyPrinter
    extends DefaultHandler
{
    public static String format (String XML, String baseURI)
        throws IOException, SAXException, ParserConfigurationException
    {
        SAXParserFactory factory =
            SAXParserFactory.newInstance ();
        factory.setFeature
            ("http://xml.org/sax/features/namespace-prefixes", true);
        factory.setValidating (false);

        PrettyPrinter pretty = new PrettyPrinter ();
        SAXParser parser = factory.newSAXParser ();
        parser.parse (new ByteArrayInputStream (XML.getBytes ()),
            pretty, baseURI);
        return pretty.toString ();
    }

    /**
    Call this to get the formatted XML post-parsing.
    */
    public String toString ()
    {
        return output.toString ();
    }

    /**
    Prints a blank line at the end of the reformatted document.
    */
    public void endDocument () throws SAXException
    {
        output.append (endLine);
    }

    /**
    Writes the start tag for the element.
    Attributes are written out, one to a text line.  Starts gathering
    character data for the element.
    */
    public void startElement
            (String URI, String name, String qName, Attributes attributes)
        throws SAXException
    {
        output.append (endLine)
              .append (indent)
              .append ('<')
              .append (qName);

        int length = attributes.getLength ();
        for (int a = 0; a < length; ++a)
            output.append (endLine)
                  .append (indent)
                  .append (standardIndent)
                  .append (attributes.getQName (a))
                  .append ("=\"")
                  .append (attributes.getValue (a))
                  .append ('\"');

        if (length > 0)
            output.append (endLine)
                  .append (indent);

        output.append ('>');

        indent += standardIndent;
        currentValue = new StringBuffer ();
    }

    /**
    Checks the {@link #currentValue} buffer to gather element content.
    Writes this out if it is available.  Writes the element end tag.
    */
    public void endElement (String URI, String name, String qName)
        throws SAXException
    {
        indent = indent.substring
            (0, indent.length () - standardIndent.length ());

        if (currentValue == null)
        {
            currentValue = new StringBuffer ();
            currentValue.append (endLine)
                        .append (indent);
        }

        currentValue.append ("</")
                    .append (qName)
                    .append ('>');

        output.append (currentValue.toString ());
        currentValue = null;
    }

    /**
    When the {@link #currentValue} buffer is enabled, appends character
    data into it, to be gathered when the element end tag is encountered.
    */
    public void characters (char[] chars, int start, int length)
        throws SAXException
    {
        if (currentValue != null)
            currentValue.append (escape (chars, start, length));
    }

    /**
    Filter to pass strings to output, escaping <b>&lt;</b> and <b>&amp;</b>
    characters to &amp;lt; and &amp;amp; respectively.
    */
    private static String escape (char[] chars, int start, int length)
    {
        StringBuffer result = new StringBuffer ();
        for (int c = start; c < start + length; ++c)
            if (chars[c] == '<')
                result.append ("&lt;");
            else if (chars[c] == '&')
                result.append ("&amp;");
            else
                result.append (chars[c]);

        return result.toString ();
    }

    /**
    This whitespace string is expanded and collapsed to manage the output
    indenting.
    */
    private String indent = "";

    /**
    A buffer for character data.  It is &quot;enabled&quot; in
    {@link #startElement startElement} by being initialized to a
    new <b>StringBuffer</b>, and then read and reset to
    <code>null</code> in {@link #endElement endElement}.
    */
    private StringBuffer currentValue = null;

    /**
    The primary buffer for accumulating the formatted XML.
    */
    private StringBuffer output = new StringBuffer ();

    private static final String endLine =
        System.getProperty ("line.separator");
    private static final String standardIndent = "  ";
}

