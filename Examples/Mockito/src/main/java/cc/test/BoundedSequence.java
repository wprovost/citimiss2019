/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.test;

/**
Callback interface by which to receive a sequence of numbers.

@author Will Provost
*/
public interface BoundedSequence
{
    /**
    Called to begin the sequence.
    */
    public void start ();

    /**
    Called once for each number in the sequence.
    */
    public void next (int number);

    /**
    Called to signal that the sequence is complete.
    */
    public void end ();
}
